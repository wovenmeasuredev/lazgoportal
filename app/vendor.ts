﻿
// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';

// RxJS
import 'rxjs';
import 'jquery';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...


import '@ng-bootstrap/ng-bootstrap';
import 'ng2-img-cropper'; 
import * as moment from 'moment';
import 'ngx-pagination';
import 'ng2-page-scroll/ng2-page-scroll';
import 'ngx-bootstrap';
import 'angular-date-value-accessor';
import 'primeng/primeng';
import 'core-js';
import 'Quill';
import 'ngx-bootstrap';
import * as _ from 'underscore';

