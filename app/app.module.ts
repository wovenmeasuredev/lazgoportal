import { NgModule, APP_INITIALIZER} from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule  } from "@angular/platform-browser/animations";
import { FormsModule }   from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImageCropperModule } from 'ng2-img-cropper';
import { AppComponent }   from "./app.component";
import { SafePipe } from './common/pipes/pipes';
import { MomentPipe } from './common/pipes/pipes';
import { FormatDatePipe } from './common/pipes/pipes';
import { HttpModule, JsonpModule } from "@angular/http";
import { HeaderComponent } from "./shared/header/header";
import { Constants } from "./common/constants";
import { NgxPaginationModule } from 'ngx-pagination';
import {Ng2PageScrollModule} from 'ng2-page-scroll/ng2-page-scroll';
import { WVDatepickerModule } from './shared/datepicker/datepicker.module';
import { DateValueAccessorModule } from 'angular-date-value-accessor';
import {
    InputTextModule, DataTableModule, ButtonModule, DialogModule, GrowlModule, DropdownModule, ColorPickerModule, ConfirmDialogModule, ConfirmationService,
    CalendarModule, EditorModule, SharedModule, InputMaskModule, TabViewModule, TooltipModule, AccordionModule, MultiSelectModule, TreeModule, TreeNode
} from 'primeng/primeng';
 
import { TabsModule, ModalModule  } from 'ngx-bootstrap';

import { CurrencyMaskModule } from "ng2-currency-mask";

/*************************************************************Service*******************************************************************/
import { AuthenticationService } from "./common/services/authentication-service";
import { SpinnerService } from "./common/services/spinner-service";
import { LoggerService } from "./common/services/logger-service";
import { ContextService } from "./common/services/context-service";
import { ProxyService } from "./common/services/proxy-service";
import { TranslationService } from "./common/services/translation-service";
import { ResetPasswordService} from "./resetPassword/resetPassword-service";
import { ChangePasswordService} from "./changePassword/changePassword-service";
import { LookupService } from "./common/services/lookup-service";
import { DataService } from "./common/services/data-service";

/*************************************************************Components*******************************************************************/
import { routing, appRoutingProviders } from "./app.routing";
import { FileUploadComponent } from "./shared/fileupload/fileupload";
import { LoginComponent } from "./login/login"; 
import { ResetPasswordComponent } from "./resetPassword/resetPassword";
import { ChangePasswordComponent } from './changePassword/changePassword'
import { ImageCropperComponent } from 'ng2-img-cropper';
import { MasterPageComponent } from './shared/masterpage/masterpage';

import { ReportListComponent } from './reports/reports-list';
import { LocationListComponent } from './location/location-list';
import { LocationDetailComponent } from './location/location-detail';
import { AddInstructionComponent } from './location/add-instruction';
import { SendRequestComponent } from './location/send-request';
import { AddEditProductComponent } from './location/add-edit-product';
import { AddEditRateComponent } from './location/add-edit-rate';
import { AddEditInventoryComponent } from './location/add-edit-inventory';
import { RateFlatPriceComponent } from './location/rate-flat-price';
import { RateTimedPriceComponent } from './location/rate-timed-price';
import { AddEditBlackoutComponent } from './location/add-edit-blackout';
import { SalesChannelListComponent } from './admin/sales-channel-list';

/*************************************************************Directives/dto*******************************************************************/
import { CanActivateGuard } from "./app.authguard"; 
import { PasswordValidator } from './changePassword/changePassword.directive';
import { CommunicationComponent } from "./location/communications";
import { InventoryListComponent } from "./location/inventory-list";
import { BlackoutsComponent } from "./location/blackouts";
import { AddEditSalesChannelComponent } from "./admin/add-edit-sales-channel";
import { OrganizationListComponent } from './admin/organization-list';
import { OrganizationDetailComponent } from './admin/organization-detail';
import { UsersComponent } from './admin/users';
import { AddEditUserComponent } from "./admin/add-edit-user";
import { AdjustmentReasonListComponent } from "./admin/adjustment-reason-list";

@NgModule({
    imports: [BrowserAnimationsModule, BrowserModule, routing, HttpModule, JsonpModule, FormsModule, NgxPaginationModule, Ng2PageScrollModule.forRoot(), NgbModule.forRoot(), WVDatepickerModule.forRoot(), DateValueAccessorModule,
        DataTableModule, GrowlModule, DropdownModule, CalendarModule, TooltipModule, ColorPickerModule, MultiSelectModule, ConfirmDialogModule, DialogModule, EditorModule, SharedModule, InputMaskModule, CurrencyMaskModule, TabViewModule, AccordionModule,
        TabsModule.forRoot(), ModalModule.forRoot(), TreeModule],
    declarations: [ 
        AppComponent, SafePipe, MomentPipe, FormatDatePipe, HeaderComponent, ImageCropperComponent,
        MasterPageComponent, LocationListComponent, LocationDetailComponent, AddInstructionComponent,
        LoginComponent, ResetPasswordComponent, ChangePasswordComponent, SendRequestComponent,
        PasswordValidator, FileUploadComponent, ReportListComponent, AddEditProductComponent, 
        AddEditRateComponent, RateFlatPriceComponent, RateTimedPriceComponent, CommunicationComponent,
        AddEditInventoryComponent, InventoryListComponent, BlackoutsComponent, AddEditBlackoutComponent,
        SalesChannelListComponent, AddEditSalesChannelComponent, OrganizationListComponent, OrganizationDetailComponent,
        UsersComponent, AddEditUserComponent, AdjustmentReasonListComponent
       ],

    entryComponents: [AddInstructionComponent, SendRequestComponent, AddEditProductComponent, AddEditRateComponent, AddEditInventoryComponent,
        AddEditBlackoutComponent, AddEditSalesChannelComponent, AddEditUserComponent],

    bootstrap: [AppComponent],

    providers: [
        appRoutingProviders, ResetPasswordService, 
        TranslationService,
        {
            provide: APP_INITIALIZER,
            deps: [TranslationService, ProxyService],
            useFactory: (translationServ: TranslationService) => () => translationServ.init(),
            multi: true
        },
        ChangePasswordService, ContextService, ProxyService,
        AuthenticationService, SpinnerService, LoggerService, DataService,
        Constants, CanActivateGuard, 
        LookupService]
})

export class AppModule { }
 