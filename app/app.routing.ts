/// <reference path="../typings/index.d.ts" />
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login';
import { ChangePasswordComponent } from './changePassword/changePassword';
import { ResetPasswordComponent } from './resetPassword/resetPassword';
import { MasterPageComponent } from './shared/masterpage/masterpage';
import { CanActivateGuard } from './app.authguard';
import { ReportListComponent } from './reports/reports-list';
import { LocationListComponent } from './location/location-list';
import { LocationDetailComponent } from './location/location-detail';
import { SalesChannelListComponent } from "./admin/sales-channel-list";
import { OrganizationListComponent } from "./admin/organization-list";
import { OrganizationDetailComponent } from "./admin/organization-detail";
import { AdjustmentReasonListComponent } from "./admin/adjustment-reason-list";
import { UsersComponent } from "./admin/users";

const appRoutes: Routes = [
    { path: '', component: MasterPageComponent, canActivate: [CanActivateGuard],
        children:  
        [
            {
                path: 'reports', component: null, canActivate: [CanActivateGuard], children:
                [
                    { path: 'report-list', component: ReportListComponent, canActivate: [CanActivateGuard] }
                ]
            },
            {
                path: 'location', component: null, canActivate: [CanActivateGuard], children:
                [
                    { path: 'location-list', component: LocationListComponent, canActivate: [CanActivateGuard] },
                    { path: 'location-detail', component: LocationDetailComponent, canActivate: [CanActivateGuard] }
                ]
            },
            {
                path: 'admin', component: null, canActivate: [CanActivateGuard], children:
                [
                    { path: 'adjustment-reason-list', component: AdjustmentReasonListComponent, canActivate: [CanActivateGuard] },
                    { path: 'sales-channel-list', component: SalesChannelListComponent, canActivate: [CanActivateGuard] },
                    { path: 'organization-list', component: OrganizationListComponent, canActivate: [CanActivateGuard] },
                    { path: 'organization-detail', component: OrganizationDetailComponent, canActivate: [CanActivateGuard] },
                    { path: 'users', component: UsersComponent, canActivate: [CanActivateGuard] }
                ]
            },
        ]
    },
    { path: 'login', component: LoginComponent},
    { path: 'resetPassword', component: ResetPasswordComponent },
    { path: 'changePassword', component: ChangePasswordComponent }   
];
 
export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
