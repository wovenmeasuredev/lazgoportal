﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { LocationLite, SalesChannel } from '../dto';
import { AddEditSalesChannelComponent } from "./add-edit-sales-channel";
import { AdjustmentReason } from "../dto/adjustment-reason";

@Component({ 
    templateUrl: 'adjustment-reason-list.html'
})
export class AdjustmentReasonListComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private ngbModal: NgbModal,
                private translationService: TranslationService) {

    }    

    terminated: boolean = false;
    msgs: Message[] = [];
    adjustmentReasons: Array<AdjustmentReason>;
  
    ngOnInit() {
        this.contextService.currentSection = "admin-adjustmentreasons";
        this.load();
    }       

    onSelectionChange() {
        this.load();
    }

    lookupRowStyleClass(rowData) {
        return '';
    }

    load() {
        this.adjustmentReasons = new Array<AdjustmentReason>();
        this.spinnerService.postStatus('Loading Adjustment Reasons');
        this.dataService.getAdjustmentReasons().subscribe(
            data => {
                this.adjustmentReasons = data;
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    } 

    saveReason(reason) {
        reason.edit = false;
    }

    removeReason(reason: AdjustmentReason) {
        var index = this.adjustmentReasons.indexOf(reason);
        this.adjustmentReasons.splice(index, 1);
    }  

    addReason() {
        let ad: AdjustmentReason = new AdjustmentReason();
        ad.edit = true;
        this.adjustmentReasons.push(ad);
    }
}