﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';
import * as _ from 'underscore';
import { Product, Rate, User } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { FlatRate } from "../dto/flat-rate";
import { Blackout } from "../dto/blackout";

@Component({ 
    templateUrl: 'users.html',
    selector: 'users'
})
export class UsersComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private translationService: TranslationService) {
    }

    @Input()
    users: Array<User>;
  
    ngOnInit() {
        if (!this.users) {
            this.contextService.currentSection = "admin-users";
            this.load();
        }
    }

    load() {
        this.users = new Array<User>();
        this.spinnerService.postStatus('Loading Users');
        this.dataService.getUsers().subscribe(
            data => {
                this.users = data;
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    } 

    formatRoles(user: User) {
        return _.pluck(user.roles, "roleName").join();
    }
    
}