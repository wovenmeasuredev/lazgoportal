﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { LocationLite, SalesChannel } from '../dto';
import { AddEditSalesChannelComponent } from "./add-edit-sales-channel";

@Component({ 
    templateUrl: 'sales-channel-list.html'
})
export class SalesChannelListComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private ngbModal: NgbModal,
                private translationService: TranslationService) {

    }    

    terminated: boolean = false;
    msgs: Message[] = [];
    salesChannels: Array<SalesChannel>;
    localStorageGridOptionsKey: string;
    gridOptions = {
        first: 0,
        rows: 10,
        sortField: 'title',
        sortOrder: 1,
        filter: ''
    };
    addModal: any;


    @ViewChild(DataTable) dataTable: DataTable;
    loadGridSortOptions() {
        var saved = this.contextService.getGridOption(this.localStorageGridOptionsKey);
        if (saved) {
            setTimeout(() => {
                this.gridOptions = saved;
                this.dataTable.globalFilter.value = this.gridOptions.filter;
                this.dataTable.sortField = this.gridOptions.sortField;
                this.dataTable.sortOrder = this.gridOptions.sortOrder;                
            }, 0);
        }
        else {
            this.gridOptions.first = 1;
            this.gridOptions.rows = 20;
        }

    }
    loadGridPageOptions() {
        var saved = this.contextService.getGridOption(this.localStorageGridOptionsKey);
        if (saved) {
            setTimeout(() => {
                this.dataTable.filter("", "", "");
                this.dataTable.paginate();               
            }, 0);
        }
        else {
            this.gridOptions.first = 1;
            this.gridOptions.rows = 20;
        }

    }

    onSort(e: { field: string, order: number }) {
        this.gridOptions.sortField = e.field;
        this.gridOptions.sortOrder = e.order;
        this.gridOptions.first = 0;
        this.contextService.setGridOption(this.localStorageGridOptionsKey, this.gridOptions);
    }

    onPage(e: { first: number, rows: number }) {
        this.gridOptions.rows = e.rows;
        this.gridOptions.first = e.first;
        this.contextService.setGridOption(this.localStorageGridOptionsKey, this.gridOptions);
    }

    onFilter(e) {
        this.gridOptions.filter = this.dataTable.globalFilter.value;
        this.contextService.setGridOption(this.localStorageGridOptionsKey, this.gridOptions);
    }

    ngOnInit() {
        this.localStorageGridOptionsKey = "saleschannellist";
        this.contextService.currentSection = "admin-saleschannel";
        this.load();
    }       

    onSelectionChange() {
        this.load();
    }

    lookupRowStyleClass(rowData) {
        return '';
    }

    load() {
        this.salesChannels = new Array<SalesChannel>();
        this.loadGridSortOptions();
        this.spinnerService.postStatus('Loading Sales Channels');
        this.dataService.getSalesChannels().subscribe(
            data => {
                this.salesChannels = data;
                this.loadGridPageOptions();
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    } 

    addSalesChannel() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditSalesChannelComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.salesChannel = new SalesChannel();

        this.addModal.result.then(function (salesChannel: SalesChannel) {
            if (salesChannel) {
                console.log(salesChannel);
                _self.salesChannels.push(salesChannel);
            }
        });
    }

    editSalesChannel(salesChannel: SalesChannel) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditSalesChannelComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.salesChannel = salesChannel;


        this.addModal.result.then(function (salesChannel: SalesChannel) {
            _self.dataService.saveSalesChannel(salesChannel).subscribe(
                data => {

                },
                (err) => { console.log(err); },
                () => {
                    _self.spinnerService.finishCurrentStatus();
                });
        });


    }

    removeSalesChannel(salesChannel: SalesChannel) {
        this.dataService.deleteSalesChannel(salesChannel).subscribe(
            data => {
                let index: number = this.salesChannels.indexOf(salesChannel);
                this.salesChannels.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }
    
}