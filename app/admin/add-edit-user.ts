﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';
import * as _ from 'underscore';
import { Product, Rate, User, Organization } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { ProductInventory } from "../dto/product-inventory";
import { InventoryBase } from "../dto/inventory";
import { LocationInventory } from "../dto/location-inventory";

@Component({ 
    templateUrl: 'add-edit-user.html'
})
export class AddEditUserComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {
        this.user = new User();
      
    }   
    
    private user: User;
    private allOrganizations: Array<SelectItem>;
    private allRoles: Array<SelectItem>;
    private allLocations: Array<SelectItem>;

    ngOnInit() {
        this.spinnerService.postStatus('Loading');
        this.dataService.getOrganizations().subscribe(
            data => {
                this.allOrganizations = _.map(_.filter(data, (d: Organization) => { return d.isLocation; }), (o: Organization) => { return { value: o.name, label: o.name } });
                this.allLocations = _.map(_.filter(data, (d: Organization) => { return !d.isLocation; }), (o: Organization) => { return { value: o.name, label: o.name } });
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    

        this.dataService.getRoles().subscribe(
            data => {
                this.allRoles = _.map(data, (r: string) => { return { value: r, label: r } });
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });


        
    }
    
    saveBlackout() {
        if (this.user) {
            this.dataService.saveUser(this.user).subscribe(
                data => {
                    this.activeModal.close(data);
                },
                (err) => { console.log(err); },
                () => {
                    this.spinnerService.finishCurrentStatus();
                });
        }     
    }

    close() {
        this.activeModal.close();
    }


}