﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable, TreeNode } from 'primeng/primeng';

import { Organization, SalesChannel, OrganizationLite } from '../dto';
import { AddEditSalesChannelComponent } from "./add-edit-sales-channel";
import * as _ from 'underscore';

@Component({ 
    templateUrl: 'organization-list.html'
})
export class OrganizationListComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private ngbModal: NgbModal,
                private translationService: TranslationService) {
        this.searchTerm = "";

    }    

    msgs: Message[] = [];
    organizations: Array<OrganizationLite>;
    
    addModal: any;

   

    ngOnInit() {
        this.contextService.currentSection = "admin-organization";
        this.load();
    }       
    

    load() {
        this.organizations = new Array<Organization>();
        this.spinnerService.postStatus('Loading Organizations');
        this.dataService.getOrganizations().subscribe(
            data => {
                this.organizationTreeNodes = new Array<TreeNode>();
                this.organizations = data;
                this.buildTreeNodes();
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    } 

    buildTreeNodes() {
        this.organizationTreeNodes = new Array<TreeNode>();
        var _self = this;
        _.each(this.organizations, (org) => {
            if (!org.parentOrganizationId) {
                this.buildRootNode(org);
            }                
        });
    }

    selectedNode: TreeNode;
    organizationTreeNodes: TreeNode[];
    searchTerm: string;


    buildRootNode(organization: OrganizationLite) {
        var treeNode: any = {
            label: organization.name,
            data: organization.organizationId,
            children: new Array<TreeNode>(),
            selectable: false
        }

        this.organizationTreeNodes.push(treeNode);
        this.addChildren(treeNode);       

    }

    addChildren(orgTreeNode: TreeNode) {
        var _self = this;
        var children = _.filter(this.organizations, (org: OrganizationLite) =>
        {
            return org.parentOrganizationId == orgTreeNode.data && (org.type != "Location" || _self.searchTerm == "" || org.name.indexOf(_self.searchTerm) >= 0);
        });

        if (children.length == 0) {
            orgTreeNode.selectable = true;
            return;
        }

        _.each(children, (org: OrganizationLite) => {
            var treeNode: any = {
                label: org.name,
                data: org.organizationId,
                children: new Array<TreeNode>(),
                selectable: false
            }
            orgTreeNode.children.push(treeNode);

            this.addChildren(treeNode);
        });

    }

    searchKeyDown() {
        this.buildTreeNodes();
    }

    nodeSelect(event) {
        this.router.navigate(["/admin/organization-detail"], { queryParams: { i: event.node.data } });
    }
    /*
    addSalesChannel() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditSalesChannelComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.salesChannel = new SalesChannel();

        this.addModal.result.then(function (salesChannel: SalesChannel) {
            if (salesChannel) {
                console.log(salesChannel);
                _self.salesChannels.push(salesChannel);
            }
        });
    }

    editSalesChannel(salesChannel: SalesChannel) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditSalesChannelComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.salesChannel = salesChannel;


        this.addModal.result.then(function (salesChannel: SalesChannel) {
            _self.dataService.saveSalesChannel(salesChannel).subscribe(
                data => {

                },
                (err) => { console.log(err); },
                () => {
                    _self.spinnerService.finishCurrentStatus();
                });
        });


    }

    removeSalesChannel(salesChannel: SalesChannel) {
        this.dataService.deleteSalesChannel(salesChannel).subscribe(
            data => {
                let index: number = this.salesChannels.indexOf(salesChannel);
                this.salesChannels.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }
    */
}