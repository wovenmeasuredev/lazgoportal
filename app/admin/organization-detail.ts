﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import { LookupService } from '../common/services/lookup-service';
import { Constants } from "../common/constants";
import { DataService } from "../common/services/data-service";
import { ProxyService } from "../common/services/proxy-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message, ConfirmationService, ConfirmDialogModule, SelectItem } from 'primeng/primeng';
import { Organization, Product, LocationDetail, LocationEmailSetup, LocationInstruction, LocationCannedInstructionByLanguage, LocationInstructionByLanguage, Rate, OrganizationLite, User } from '../dto';
import * as _ from 'underscore';
import { AddEditUserComponent } from "./add-edit-user";
import { LanguageField } from "../dto/language-field";

@Component({
    templateUrl: 'organization-detail.html',
    providers: [ConfirmationService]
})
export class OrganizationDetailComponent {   
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private lookupService: LookupService,
                private ngbModal: NgbModal,
                private translationService: TranslationService,
                private dataService: DataService,
                private confirmationService: ConfirmationService) {

       
    }    


    msgs: Message[] = [];
    organizationId: any;
    states: SelectItem[];
    regions: SelectItem[];
    allParents: Array<OrganizationLite>;
    organization: Organization;

    addModal: any;

    selectedLanguage: string;

    ngOnInit() {
        this.contextService.currentSection = "admin-organization";
        this.organizationId = this.route.snapshot.queryParams['i'];
        this.loadOrganizationDetail();
        this.loadStates();
        this.selectedLanguage = "en";
    }   

    getLanguageFields() : Array<LanguageField> {
        return this.organization.languageFields.get(this.selectedLanguage);
       
    }

    loadStates() { 
        this.states = [];
        this.states.push({ label: 'Select State', value: null });
        for (let n: number = 0; n < this.constants.US_States.length; n++) {
            this.states.push({ label: this.constants.US_States[n].state, value: this.constants.US_States[n].postCode });
        }
    }
        
    loadOrganizationDetail() {
        this.spinnerService.postStatus('Loading Organization');

        this.dataService.getOrganizations().subscribe(
            data => {
                this.allParents = _.filter(data, (org: OrganizationLite) => {
                    return org.organizationId != this.organizationId && org.type != 'Location';
                });
                this.dataService.getOrganization(this.organizationId).subscribe(
                    data => {
                        this.organization = data;
                    },
                    (err) => { console.log(err); },
                    () => {
                        this.spinnerService.finishCurrentStatus();
                    });    

            },
            (err) => { console.log(err); }, 
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    }      

    saveOrganization() {

    }


    addingUser() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditUserComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.user = new User();

        this.addModal.result.then(function (user: User) {
            if (user) {
                console.log(user);
                _self.organization.users.push(user);
            }
        });
    }

    editingUser() {
    }

    deletingUser() {

    }
}