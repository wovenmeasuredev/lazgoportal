import { Injectable } from '@angular/core';
import { Language } from '../dto';


@Injectable() 
export class Constants {
    public get BaseApiUri(): string { return "http://api-laz.wovenmeasure.com/api/"; }
    public get BaseApiUriLocal(): string { return "http://laz.webapi/api/"; }

    public alertStatusNew: number = 1; 

    public get US_States() {
        var states = [
            { state: 'Alabama', postCode: 'AL' }, { state: 'Alaska', postCode: 'AK' }, { state: 'Arizona', postCode: 'AZ' }, { state: 'Arkansas', postCode: 'AR' }, { state: 'California', postCode: 'CA' },
            { state: 'Colorado', postCode: 'CO' }, { state: 'Connecticut', postCode: 'CT' }, { state: 'Delaware', postCode: 'DE' }, { state: 'District Of Columbia', postCode: 'DC' },
            { state: 'Florida', postCode: 'FL' }, { state: 'Georgia', postCode: 'GA' }, { state: 'Hawaii', postCode: 'HI' }, { state: 'Idaho', postCode: 'ID' }, { state: 'Illinois', postCode: 'IL' },
            { state: 'Indiana', postCode: 'IN' }, { state: 'Iowa', postCode: 'IA' }, { state: 'Kansas', postCode: 'KS' }, { state: 'Kentucky', postCode: 'KY' }, { state: 'Louisiana', postCode: 'LA' },
            { state: 'Maine', postCode: 'ME' }, { state: 'Maryland', postCode: 'MD' }, { state: 'Massachusetts', postCode: 'MA' }, { state: 'Michigan', postCode: 'MI' }, { state: 'Minnesota', postCode: 'MN' },
            { state: 'Mississippi', postCode: 'MS' }, { state: 'Missouri', postCode: 'MO' }, { state: 'Montana', postCode: 'MT' }, { state: 'Nebraska', postCode: 'NE' }, { state: 'Nevada', postCode: 'NV' },
            { state: 'New Hampshire', postCode: 'NH' }, { state: 'New Jersey', postCode: 'NJ' }, { state: 'New Mexico', postCode: 'NM' }, { state: 'New York', postCode: 'NY' },
            { state: 'North Carolina', postCode: 'NC' }, { state: 'North Dakota', postCode: 'ND' }, { state: 'Ohio', postCode: 'OH' }, { state: 'Oklahoma', postCode: 'OK' }, { state: 'Oregon', postCode: 'OR' },
            { state: 'Pennsylvania', postCode: 'PA' }, { state: 'Rhode Island', postCode: 'RI' }, { state: 'South Carolina', postCode: 'SC' }, { state: 'South Dakota', postCode: 'SD' },
            { state: 'Tennessee', postCode: 'TN' }, { state: 'Texas', postCode: 'TX' }, { state: 'Utah', postCode: 'UT' }, { state: 'Vermont', postCode: 'VT' }, { state: 'Virginia', postCode: 'VA' },
            { state: 'Washington', postCode: 'WA' }, { state: 'West Virginia', postCode: 'WV' }, { state: 'Wisconsin', postCode: 'WI' }, { state: 'Wyoming', postCode: 'WY' }
        ];
         
        return states;
    }

    public get LocationInstructionSectionPassRequirements(): number { return 1; }
    public get LocationInstructionSectionSpecialInstructions(): number { return 2; }
    public get LocationInstructionSectionLocationInstructions(): number { return 3; }

    public get PassTypeSmartPhone(): string { return 'spp' };
    public get PassTypeAppleWallet(): string { return 'awp' };
    public get PassTypeColorsPrintedPass(): string { return 'cpp' };
    public get PassTypeColorsHardPass(): string { return 'chp' };
    public get PassTypeNoPassRequired(): string { return 'npr' };

    public get EmailOptionDefault(): string { return 'd' };
    public get EmailOptionCustom(): string { return 'c' };

    public get MessageOptionDefault(): string { return 'd' };
    public get MessageOptionCustom(): string { return 'c' };


    /*languages*/
    public get LanguageEnglish(): Language { return { code: "en", description: "English" } };
    public get LanguageSpanish(): Language { return { code: "es", description: "Spanish" } };

    /*rates*/
    public get RateTypeTimed(): number { return 1 };
    public get RateTypeFlat(): number { return 2 };

}

