﻿
export class ContextInfo {
    public userName: string;
    public email: string;
    public token: string;
    public tokenExpires: Date;
    public displayName: string;
    public fullName: string;
    public firstName: string;
    public lastName: string;
    public userId: string;
    
    constructor() {
    }
}
