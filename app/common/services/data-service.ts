﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Constants } from '../constants';
import { Rate, SalesChannel, Product, LocationDetail, LocationLite, LocationEmailSetup, LocationInstruction, LocationInstructionByLanguage, LocationCannedInstructionByLanguage, PassTypeOption, CommunicationSetup, SalesChannelProduct, OrganizationLite, Organization, User, Role } from '../../dto';
import { RateNameByLanguage } from "../../dto/rate-name-by-language";
import { FlatRate } from "../../dto/flat-rate";
import { TimedRate } from "../../dto/timed-rate";
import { ProductInventory } from "../../dto/product-inventory";
import { LocationInventory } from "../../dto/location-inventory";
import { Blackout } from "../../dto/blackout";
import { LanguageField } from "../../dto/language-field";
import { AdjustmentReason } from "../../dto/adjustment-reason";
@Injectable()
export class DataService {
    constructor(private constants: Constants) { }

    createRandomId(): string {
        return (Math.random() + 1).toString(36).substring(10);
    }

    getPermissions(): string[] {
        let retVal: string[] = new Array<string>();

        retVal.push("Financial");
        retVal.push("Parking Operations");
        retVal.push("Attendant");

        return retVal;
    }

    getUsers(): Observable<User[]> {
        let retVal: Array<User> = new Array<User>();

        let user: User = new User();
        user.userId = this.createRandomId();
        user.userName = "john201";
        user.fullName = "John Smith";
        user.roles = new Array<Role>()

        let r: Role = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "Admin";
        r.permissions = this.getPermissions();
        user.roles.push(r);

        r = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "Corporate Manager";
        r.permissions = this.getPermissions();
        user.roles.push(r);
        retVal.push(user);


        user = new User();
        user.userId = this.createRandomId();
        user.userName = "smitty2012";
        user.fullName = "Mike Jones";
        user.roles = new Array<Role>()

        r = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "Admin";
        r.permissions = this.getPermissions();
        user.roles.push(r);

        r = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "System Export Manager";
        r.permissions = new Array<string>();
        r.permissions.push(this.getPermissions()[2]);
        user.roles.push(r);
        retVal.push(user);

        return Observable.of(retVal);
    }

    getAdjustmentReasons(): Observable<AdjustmentReason[]> {
        let retVal: Array<AdjustmentReason> = new Array<AdjustmentReason>();

        let adj: AdjustmentReason = new AdjustmentReason();
        adj.adjustmentReasonId = this.createRandomId();
        adj.reason = "Customer uphappy with spot.";
        retVal.push(adj);

        adj = new AdjustmentReason();
        adj.adjustmentReasonId = this.createRandomId();
        adj.reason = "Customer uphappy with service.";
        retVal.push(adj);

        return Observable.of(retVal);
    }

    getInstructionSuggestions(): Observable<LocationCannedInstructionByLanguage[]> {
        let retVal: Array<LocationCannedInstructionByLanguage> = new Array<LocationCannedInstructionByLanguage>();

        let locationCannedInstructionByLanguage = new LocationCannedInstructionByLanguage();
        locationCannedInstructionByLanguage.instructionByLanguage = new Array<LocationInstructionByLanguage>();
        locationCannedInstructionByLanguage.sequence = 1;

        let ins = new LocationInstructionByLanguage();
        ins.instruction = "The gate will open and you can park anywhere that doesn't say 'reserved'.";
        ins.language = this.constants.LanguageEnglish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);
        
        ins = new LocationInstructionByLanguage();
        ins.instruction = "La puerta se abrirá y se puede aparcar en cualquier lugar que no diga 'reservado'.";
        ins.language = this.constants.LanguageSpanish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);
        retVal.push(locationCannedInstructionByLanguage);

        locationCannedInstructionByLanguage = new LocationCannedInstructionByLanguage();
        locationCannedInstructionByLanguage.instructionByLanguage = new Array<LocationInstructionByLanguage>();
        locationCannedInstructionByLanguage.sequence = 2;

        ins = new LocationInstructionByLanguage();
        ins.instruction = "To Exit: Press the intercom button.  Have your Parking Pass AND entry ticket numbers for the agent to validate.";
        ins.language = this.constants.LanguageEnglish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);

        ins = new LocationInstructionByLanguage();
        ins.instruction = "Para salir: Pulse el botón de intercomunicación.Haga que su pase de estacionamiento Y los números de entrada de entrada para que el agente de validar.";
        ins.language = this.constants.LanguageSpanish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);
        retVal.push(locationCannedInstructionByLanguage);

        locationCannedInstructionByLanguage = new LocationCannedInstructionByLanguage();
        locationCannedInstructionByLanguage.instructionByLanguage = new Array<LocationInstructionByLanguage>();
        locationCannedInstructionByLanguage.sequence = 3;

        ins = new LocationInstructionByLanguage();
        ins.instruction = "The gate will open and you can park anywhere that doesn't say 'reserved'.";
        ins.language = this.constants.LanguageEnglish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);

        ins = new LocationInstructionByLanguage();
        ins.instruction = "Para salir: Pulse el botón de intercomunicación.Haga que su pase de estacionamiento Y los números de entrada de entrada para que el agente de validar.";
        ins.language = this.constants.LanguageSpanish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);
        retVal.push(locationCannedInstructionByLanguage);

        locationCannedInstructionByLanguage = new LocationCannedInstructionByLanguage();
        locationCannedInstructionByLanguage.instructionByLanguage = new Array<LocationInstructionByLanguage>();
        locationCannedInstructionByLanguage.sequence = 4; 

        ins = new LocationInstructionByLanguage();
        ins.instruction = "Hold your Parking Pass barcode under the scanner.  Take a ticket and keep it for when you exit.";
        ins.language = this.constants.LanguageEnglish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);

        ins = new LocationInstructionByLanguage();
        ins.instruction = "Mantenga su código de barras de Pase de estacionamiento debajo del escáner.Tome un boleto y manténgalo para cuando salga.";
        ins.language = this.constants.LanguageSpanish;
        locationCannedInstructionByLanguage.instructionByLanguage.push(ins);
        retVal.push(locationCannedInstructionByLanguage);
        

        return Observable.of(retVal);
    }

    getSalesChannels(): Observable<SalesChannel[]> {
        return Observable.of(this.getSalesChannelsData());
    }

    getSalesChannelsData(): Array<SalesChannel> {
        let retVal: Array<SalesChannel> = new Array<SalesChannel>();
        let sc: SalesChannel = new SalesChannel();
        sc.amount = 10;
        sc.amountType = "$";
        sc.salesChannelId = this.createRandomId();
        sc.name = "Sales Channel 1";
        sc.feeType = "City Tax";
        retVal.push(sc);

        sc = new SalesChannel();
        sc.amount = 20;
        sc.amountType = "$";
        sc.salesChannelId = this.createRandomId();
        sc.name = "Sales Channel 2";
        sc.feeType = "ResFee";
        retVal.push(sc);

        sc = new SalesChannel();
        sc.amount = 30;
        sc.amountType = "%";
        sc.salesChannelId = this.createRandomId();
        sc.name = "Sales Channel 3";
        sc.feeType = "ResFee";
        retVal.push(sc);

        return retVal;
    }

    getRandomProductSalesChannels(productId: string): Array<SalesChannelProduct> {
        let retVal: Array<SalesChannelProduct> = new Array<SalesChannelProduct>();

        var scs = this.getSalesChannelsData();

        let scp: SalesChannelProduct = new SalesChannelProduct();
        scp.productId = productId;
        scp.salesChannel = scs[0];
        scp.salesChannelId = scs[0].salesChannelId;
        scp.salesChannelProductId = this.createRandomId();
        retVal.push(scp);

        scp = new SalesChannelProduct();
        scp.productId = productId;
        scp.salesChannel = scs[1];
        scp.salesChannelId = scs[1].salesChannelId;
        scp.salesChannelProductId = this.createRandomId();
        retVal.push(scp);

        return retVal;
    }



    getOrganization(organizationId: string): Observable<Organization> {
        let retVal: Organization = new Organization();
        retVal.name = "LAZ Parking";
        retVal.organizationId = this.createRandomId();
        retVal.contactPerson = "Jeff Joyce";
        retVal.parentOrganizationId = "";
        retVal.contactEmail = "jjeff@travelsciences.com";
        retVal.currencySymbol = "$";
        retVal.contactPhone = "";
        retVal.streetAddress = "";
        retVal.callCenterReservationUrl = "http://yahoo.com";
        retVal.zip = "0612";
        retVal.logoUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        retVal.dailyExportLastExportDate = new Date(2017, 2, 4);


        retVal.users.push(this.getRandomUser(retVal.name));
        retVal.users.push(this.getRandomUser(retVal.name)); 
        retVal.users.push(this.getRandomUser(retVal.name));
        retVal.users.push(this.getRandomUser(retVal.name));


        retVal.languageFields.set("en", this.getLanguageFields("en"));
        retVal.languageFields.set("es", this.getLanguageFields("es"));
        retVal.languageFields.set("fr", this.getLanguageFields("fr"));
        retVal.languageFields.set("de", this.getLanguageFields("de"));

        retVal.communicationSetup = this.getRandomCommunicationSetup();

        return Observable.of(retVal);
    }

    getLanguageFields(languageCode: string): Array<LanguageField> {
        let retVal: Array<LanguageField> = new Array<LanguageField>();

        //hardcoding a little here just to make the data look real in ui.  
        //in actuality, this will come from server/db

        if (languageCode === "en") {
            let lf: LanguageField = new LanguageField();
            lf.code = "en";
            lf.labelName = "103";
            lf.translation = "english translation for field 103";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "en";
            lf.labelName = "ADD";
            lf.translation = "add";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "en";
            lf.labelName = "158";
            lf.translation = "english translation for field 158";
            retVal.push(lf);
        }

        else if (languageCode === "es") {
            let lf: LanguageField = new LanguageField();
            lf.code = "es";
            lf.labelName = "103";
            lf.translation = "traducción en Español de Field 103";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "es";
            lf.labelName = "ADD";
            lf.translation = "Añadir";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "es";
            lf.labelName = "158";
            lf.translation = "traducción en Español de Field 158";
            retVal.push(lf);
        }

        else if (languageCode === "fr") {
            let lf: LanguageField = new LanguageField();
            lf.code = "fr";
            lf.labelName = "103";
            lf.translation = "traduction en français pour le champ 103";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "fr";
            lf.labelName = "ADD";
            lf.translation = "Ajouter";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "fr";
            lf.labelName = "158";
            lf.translation = "traduction en français pour le champ 158";
            retVal.push(lf);
        }
        else if (languageCode === "de") {
            let lf: LanguageField = new LanguageField();
            lf.code = "de";
            lf.labelName = "103";
            lf.translation = "Deutsche Übersetzung für Feld 103";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "de";
            lf.labelName = "ADD";
            lf.translation = "Hinzufügen";
            retVal.push(lf);

            lf = new LanguageField();
            lf.code = "de";
            lf.labelName = "158";
            lf.translation = "Deutsche Übersetzung für Feld 158";
            retVal.push(lf);
        }

        return retVal;
    }



    getRandomUser(organizationToAdd: string): User {
        let user: User = new User();
        user.userId = this.createRandomId();
        user.userName = 'tim' + this.createRandomId();
        user.password = "r";
        user.confirmPassword = "r";
        user.fullName = "Tim Smith" + this.createRandomId();        
        user.roles = new Array<Role>()

        let r: Role = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "Admin";
        r.permissions = this.getPermissions();
        user.roles.push(r);

        r = new Role();
        r.roleId = this.createRandomId();
        r.roleName = "Corporate Manager";
        r.permissions = this.getPermissions();
        user.roles.push(r);
        user.organizations.push(organizationToAdd)
        user.locationOrganizations.push("Atlanta");
        user.locationOrganizations.push("Savannah");
        user.locationOrganizations.push("Macon");

        return user;
    }


    //will sit between components and proxy service
    getOrganizations():Observable<OrganizationLite[]> {
        let retVal: Array<OrganizationLite> = new Array<OrganizationLite>();

        let org: OrganizationLite = new OrganizationLite();
        org.name = "LAZ Parking";
        org.organizationId = this.createRandomId();
        org.parentOrganizationId = "";
        org.type = "Company";

        let childOrg: OrganizationLite = new OrganizationLite();
        childOrg.name = "08 - Southeast";
        childOrg.organizationId = this.createRandomId();
        childOrg.parentOrganizationId = org.organizationId;
        childOrg.type = "Region";
        let seId = childOrg.organizationId;

        retVal.push(org);
        retVal.push(childOrg);


        childOrg = new OrganizationLite();
        childOrg.name = "14 - Southern California";
        childOrg.organizationId = this.createRandomId();
        childOrg.parentOrganizationId = org.organizationId;
        childOrg.type = "Region";
        retVal.push(childOrg);
        let calId = childOrg.organizationId;

        let georgiaId: string = this.createRandomId();
        let subOrg: OrganizationLite = new OrganizationLite();
        subOrg.name = "Georgia";
        subOrg.organizationId = georgiaId;
        subOrg.parentOrganizationId = seId;
        subOrg.type = "Region";
        retVal.push(subOrg);
        
        subOrg = new OrganizationLite();
        subOrg.name = "Atlanta - 250 Trinity Ave S.W.";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = georgiaId;
        subOrg.type = "Location";
        retVal.push(subOrg);

        subOrg = new OrganizationLite();
        subOrg.name = "Atlanta - Metropolis (Integrated)";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = georgiaId;
        subOrg.type = "Location";
        retVal.push(subOrg);

        subOrg = new OrganizationLite();
        subOrg.name = "Atlanta - 100 Luckie St";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = georgiaId;
        subOrg.type = "Location";
        retVal.push(subOrg); 

        subOrg = new OrganizationLite();
        subOrg.name = "Atlanta - 750 Peachtree";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = georgiaId;
        subOrg.type = "Location";
        retVal.push(subOrg);

        subOrg = new OrganizationLite();
        subOrg.name = "Tennessese";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = seId;
        subOrg.type = "Region";
        retVal.push(subOrg);



        subOrg = new OrganizationLite();
        subOrg.name = "Los Angeles North";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = calId;
        subOrg.type = "Region";
        retVal.push(subOrg);


        subOrg = new OrganizationLite();
        subOrg.name = "Los Angeles South";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = calId;
        subOrg.type = "Region";
        retVal.push(subOrg);

        var laSId = subOrg.organizationId;

        subOrg = new OrganizationLite();
        subOrg.name = "5th Blocks";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = laSId;
        subOrg.type = "Location";
        retVal.push(subOrg);

        subOrg = new OrganizationLite();
        subOrg.name = "15th Blocks";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = laSId;
        subOrg.type = "Location";
        retVal.push(subOrg);


        subOrg = new OrganizationLite();
        subOrg.name = "25th Blocks";
        subOrg.organizationId = this.createRandomId();
        subOrg.parentOrganizationId = laSId;
        subOrg.type = "Location";
        retVal.push(subOrg);


        return Observable.of(retVal);
    }    


    getLocations(): Observable<LocationLite[]> {
        let retVal: Array<LocationLite> = new Array<LocationLite>();

        let location: LocationLite = new LocationLite();
        location.city = "Atlanta";
        location.state = "GA";
        location.eDataID = "100139";
        location.GLCode = "1562";
        location.locationName = "LAZ Atlanta 100100";
        location.locationId = "1021943";
        retVal.push(location);

        location = new LocationLite();
        location.city = "Los Angeles";
        location.state = "CA";
        location.eDataID = "300139";
        location.GLCode = "3129";
        location.locationId = "28192";
        location.locationName = "LAZ Los Angeles 300100";
        retVal.push(location);

        location = new LocationLite();
        location.city = "Houston";
        location.state = "TX";
        location.eDataID = "201925";
        location.GLCode = "2023";
        location.locationName = "LAZ Houston 250197";
        location.locationId = "121831";
        retVal.push(location);

        location = new LocationLite();
        location.city = "Los Angeles";
        location.state = "TX";
        location.eDataID = "2025";
        location.GLCode = "300239";
        location.locationName = "LAZ Los Angeles 300121";
        location.locationId = "57381";
        retVal.push(location);

        return Observable.of(retVal);
    }    


    getLocation(locationId: string): Observable<LocationDetail> {
        let retVal: LocationDetail = new LocationDetail();
        retVal = new LocationDetail();

        retVal.locationName = "Wyndham Grand Chicago";
        retVal.streetAddress = "123 Main St";
        retVal.city = "Chicago";
        retVal.state = "IL";
        retVal.zip = "12345";
        retVal.generalPhone = "555-988-1234";
        retVal.helpPhone = "222-867-5309";
        retVal.email = "wyndham@lazparking.com";
        retVal.longitude = 41.887379;
        retVal.latitude = -87.625656;

        retVal.organization = "10 - Chicago & Midwest";
        retVal.organizationId = "102193";
        retVal.region = "Midwest";
        retVal.regionId = "10";
        retVal.eDataID = "1562";
        retVal.GLCode = "100139";
        retVal.status = "Active";
        retVal.statusId = "11";
        retVal.users = "Frank Smith, John Adams";

        retVal.communicationSetup = this.getRandomCommunicationSetup();

        retVal.products = new Array<Product>();

        let p: Product = new Product();
        p.productId = this.createRandomId();
        p.name = "Standard";
        p.subName = "Daily";
        p.rates = this.getRandomRates(true);
        p.type = "Standard";
        p.status = "Active";
        p.shortDescription = "Standard Daily Parking product.";
        p.longDescription = "Standard Daily Parking product long description.";
        p.rateImageUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        p.communicationSetup = this.getRandomCommunicationSetup();
        p.customizeCommunications = true;
        p.inventory = this.getProductInventory(p.productId, p.name);
        p.customizeInventory = true;
        p.salesChannels = this.getRandomProductSalesChannels(p.productId);
        retVal.products.push(p);

        p = new Product();
        p.productId = this.createRandomId();
        p.name = "Standard";
        p.subName = "Monthly";
        p.rates = this.getRandomRates(false);
        p.type = "Monthly";
        p.status = "Active"; 
        p.shortDescription = "Standard Monthly Parking product.";
        p.longDescription = "Standard Monthly Parking product long description.  Standard Monthly Parking product long description.";
        p.rateImageUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        p.communicationSetup = this.getRandomCommunicationSetup();
        p.customizeCommunications = true;
        p.inventory = this.getProductInventory(p.productId, p.name);
        p.customizeInventory = true;
        p.salesChannels = this.getRandomProductSalesChannels(p.productId);
        retVal.products.push(p);

        p = new Product();
        p.productId = this.createRandomId();
        p.name = "Early Bird";
        p.subName = "Daily";
        p.rates = this.getRandomRates(false);
        p.type = "Standard";
        p.status = "Active";
        p.shortDescription = "Standard Early Bird Daily Parking product.";
        p.longDescription = "Standard Early Bird Daily Parking product long description.  Standard Early Bird Daily Parking product long description.";
        p.rateImageUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        p.communicationSetup = this.getRandomCommunicationSetup();
        p.customizeCommunications = true;
        p.inventory = this.getProductInventory(p.productId, p.name);
        p.customizeInventory = true;
        p.salesChannels = this.getRandomProductSalesChannels(p.productId);
        retVal.products.push(p);

        p.productId = this.createRandomId();
        p.name = "Airport";
        p.subName = "Daily";
        p.rates = this.getRandomRates(false);
        p.type = "Airport Daily";
        p.status = "Active";
        p.shortDescription = "Airport Daily Parking product.";
        p.longDescription = "Airport Daily Parking product long description.  Airport Daily Parking product long description.  Airport Daily Parking product long description.";
        p.rateImageUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        p.customizeCommunications = false;
        p.customizeInventory = false;
        p.salesChannels = this.getRandomProductSalesChannels(p.productId);
        retVal.products.push(p);


        let b: Blackout = new Blackout();
        b.locationBlackoutId = this.createRandomId();
        b.locationId = locationId;
        b.startDateTime = new Date("January 1, 2018 00:00:00");
        b.endDateTime = new Date("January 2, 2018 23:59:59");
        b.reason = "Playoff game";
        retVal.blackouts.push(b);
        b = new Blackout();
        b.locationBlackoutId = this.createRandomId();
        b.locationId = locationId;
        b.startDateTime = new Date("January 15, 2018 00:00:00");
        b.endDateTime = new Date("January 16, 2018 23:59:59");
        b.reason = "Circus in town";
        retVal.blackouts.push(b);


        return Observable.of(retVal);
    }    

    getProductInventory(productId: string, name: string): Array<ProductInventory>
    {
        let retval: Array<ProductInventory> = new Array<ProductInventory>();

        let pi = new ProductInventory();
        pi.productInventoryId = this.createRandomId();
        pi.productName = name;
        pi.startDateTime = new Date("January 1, 2010 00:00:00");
        pi.endDateTime = new Date("December 31, 2070 23:59:59");
        pi.productId = productId;
        pi.monday = pi.tuesday = pi.thursday = pi.friday = pi.wednesday = true;
        pi.spaces = 74;
        retval.push(pi);

        pi = new ProductInventory();
        pi.productName = name;
        pi.startDateTime = new Date("January 1, 2010 00:00:00");
        pi.endDateTime = new Date("December 31, 2070 23:59:59");
        pi.productId = productId;
        pi.saturday = pi.sunday = true;
        pi.spaces = 85;
        retval.push(pi);

        return retval;
    }

    getRandomCommunicationSetup(): CommunicationSetup{
        let retVal: CommunicationSetup = new CommunicationSetup();
        retVal.passTypes = new Array<string>();
        retVal.passTypes.push(this.constants.PassTypeSmartPhone);
        retVal.passTypes.push(this.constants.PassTypeColorsPrintedPass);

        retVal.passUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';

        retVal.instructions = new Array<LocationInstruction>();
        let locationInstruction: LocationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "212";

        let instructionByLanguage: LocationInstructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);


        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        locationInstruction.sequence = 1;
        locationInstruction.type = "Scan In";
        locationInstruction.typeId = 1;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionPassRequirements;
        retVal.instructions.push(locationInstruction);

        locationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "214";

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);


        locationInstruction.sequence = 1;
        locationInstruction.type = "Scan In";
        locationInstruction.typeId = 1;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionSpecialInstructions;
        retVal.instructions.push(locationInstruction);

        locationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "225";

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);



        locationInstruction.sequence = 2;
        locationInstruction.type = "Scan In";
        locationInstruction.typeId = 1;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionSpecialInstructions;
        retVal.instructions.push(locationInstruction);

        locationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "235";

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.instruction = "The gate will open and you can park anywhere that doesn't say \"reserved\".";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "La puerta se abrirá y se puede aparcar en cualquier lugar que no diga \"reservado \".";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);


        locationInstruction.sequence = 1;
        locationInstruction.type = "Scan In";
        locationInstruction.typeId = 1;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionLocationInstructions;
        retVal.instructions.push(locationInstruction);

        locationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "265";

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.instruction = "Hold your parking pass under the scanner.";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "Mantenga su pase de estacionamiento bajo el escáner.";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        locationInstruction.sequence = 2;
        locationInstruction.type = "Scan In";
        locationInstruction.typeId = 1;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionLocationInstructions;
        retVal.instructions.push(locationInstruction);


        locationInstruction = new LocationInstruction();
        locationInstruction.instructionId = "525";

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageEnglish;
        instructionByLanguage.instruction = "Turn in pass when leaving garage.";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        instructionByLanguage = new LocationInstructionByLanguage();
        instructionByLanguage.locationInstructionByLanguageId = this.createRandomId();
        instructionByLanguage.language = this.constants.LanguageSpanish;
        instructionByLanguage.instruction = "Dé vuelta en paso al salir del garaje.";
        locationInstruction.instructionByLanguage.push(instructionByLanguage);

        locationInstruction.sequence = 1;
        locationInstruction.type = "Intercom Redemption";
        locationInstruction.typeId = 4;
        locationInstruction.sectionId = this.constants.LocationInstructionSectionLocationInstructions;
        retVal.instructions.push(locationInstruction);

        retVal.emailSubject = "Parking Reservation";
        retVal.emailFrom = "reservations@lazparking.com";
        retVal.emailBcc = "ecsini@lazparking.com";
        retVal.emailConfirmOption = this.constants.EmailOptionDefault;
        retVal.emailAmendmentOption = this.constants.EmailOptionDefault;
        retVal.emailCancelOption = this.constants.EmailOptionDefault;

        retVal.messageAmendmentOption = this.constants.MessageOptionDefault;
        retVal.messageCancelOption = this.constants.MessageOptionDefault;

        retVal.messageConfirmOptions = new Array<PassTypeOption>();
        retVal.messageConfirmOptions.push(new PassTypeOption(this.constants.PassTypeSmartPhone, this.constants.MessageOptionDefault));
        retVal.messageConfirmOptions.push(new PassTypeOption(this.constants.PassTypeNoPassRequired, this.constants.MessageOptionDefault));
        retVal.messageConfirmOptions.push(new PassTypeOption(this.constants.PassTypeColorsPrintedPass, this.constants.MessageOptionDefault));

        return retVal;
    }

    getProduct(productId: string) {
        let p: Product = new Product();
        p.productId = productId;
        p.name = "Standard";
        p.subName = "Daily";
        p.rates = this.getRandomRates(true);
        p.type = "Standard";
        p.status = "Active";
        p.shortDescription = "Standard Daily Parking product.";
        p.longDescription = "Standard Daily Parking product long description.";
        p.rateImageUri = 'http://www.mycurrencyexchange.com/wp-content/uploads/2014/02/2014-Vehicle-Sticker1-e1426622180837.png';
        return Observable.of(p);
    }

    getRandomRates(flatRate: boolean): Array<Rate> {
        let retVal: Array<Rate> = new Array<Rate>();

        let r: Rate = new Rate();
        r.priority = 1;
        
        let rnbl: RateNameByLanguage = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageEnglish;
        rnbl.rateName = "Rate 1";
        r.rateNameByLanguage.push(rnbl);
        rnbl = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageSpanish;
        rnbl.rateName = "Tarifa 1";
        r.rateNameByLanguage.push(rnbl);

        if (flatRate) {
            r.typeId = this.constants.RateTypeFlat;
            r.monday = true;
            r.tuesday = true;
            let fr: FlatRate = new FlatRate();
            fr.mondayValue = 10.0;
            fr.tuesdayValue = 12.0;
            r.flatRate = fr;
        }
        else {
            r.typeId = this.constants.RateTypeTimed;
            r.monday = true;
            r.tuesday = true;
            let tr: TimedRate = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 0;
            tr.to = 12;
            tr.mondayValue = 10.0;
            tr.tuesdayValue = 15.0;
            r.timedRates.push(tr);

            tr = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 12;
            tr.to = 24;
            tr.mondayValue = 17.0;
            tr.tuesdayValue = 19.0;
            r.timedRates.push(tr);
        }
        retVal.push(r);

        r = new Rate();
        r.priority = 2;
        rnbl = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageEnglish;
        rnbl.rateName = "Rate 2";
        r.rateNameByLanguage.push(rnbl);
        rnbl = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageSpanish;
        rnbl.rateName = "Tarifa 2";
        if (flatRate) {
            r.typeId = this.constants.RateTypeFlat;
            r.monday = true;
            r.tuesday = true;
            r.friday = true;
            r.saturday = true;
            let fr: FlatRate = new FlatRate();
            fr.mondayValue = 10.0;
            fr.tuesdayValue = 12.0;
            fr.fridayValue = 15.0;
            fr.saturdayValue = 20.0;
            r.flatRate = fr;
        }
        else {
            r.typeId = this.constants.RateTypeTimed;
            r.monday = true;
            r.tuesday = true;
            let tr: TimedRate = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 0;
            tr.to = 12;
            tr.mondayValue = 10.0;
            tr.tuesdayValue = 15.0;
            r.timedRates.push(tr);

            tr = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 12;
            tr.to = 24;
            tr.mondayValue = 17.0;
            tr.tuesdayValue = 19.0;
            r.timedRates.push(tr);
        }
        retVal.push(r);

        r = new Rate();
        r.priority = 3;
        rnbl = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageEnglish;
        rnbl.rateName = "Rate 3";
        r.rateNameByLanguage.push(rnbl);
        rnbl = new RateNameByLanguage();
        rnbl.rateNameByLanguageId = this.createRandomId();
        rnbl.language = this.constants.LanguageSpanish;
        rnbl.rateName = "Tarifa 3";
        if (flatRate) {
            r.typeId = this.constants.RateTypeFlat;
            r.monday = true;
            r.tuesday = true;
            r.friday = true;
            r.saturday = true;
            let fr: FlatRate = new FlatRate();
            fr.mondayValue = 10.0;
            fr.tuesdayValue = 12.0;
            fr.fridayValue = 15.0;
            fr.saturdayValue = 20.0;
            r.flatRate = fr;
        }
        else {
            r.typeId = this.constants.RateTypeTimed;
            r.monday = true;
            r.tuesday = true;
            let tr: TimedRate = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 0;
            tr.to = 12;
            tr.mondayValue = 10.0;
            tr.tuesdayValue = 15.0;
            r.timedRates.push(tr);

            tr = new TimedRate();
            tr.timedRateId = this.createRandomId();
            tr.from = 12;
            tr.to = 24;
            tr.mondayValue = 17.0;
            tr.tuesdayValue = 19.0;
            r.timedRates.push(tr);
        }
        retVal.push(r);

        return retVal;
    }

    //handles insert and update
    saveProduct(product: Product): Observable<Product> {

        //save to api
        //if productId is empty or '0', it's an insert
        if (product.productId == '' || product.productId == '0')
            product.productId = this.createRandomId(); //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
                                                   //keep close to real life (having ids in the models).
        return Observable.of(product);
    }

    deleteProduct(product: Product): Observable<Product> {
        //delete via api
        return Observable.of(product);
    }
     //handles insert and update
    saveRate(rate: Rate): Observable<Rate> {

        //save to api
        //if rateId is empty or '0', it's an insert
        if (rate.rateId == '' || rate.rateId == '0')
            rate.rateId = this.createRandomId();

        return Observable.of(rate);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
                                                   //keep close to real life (having ids in the models).
    }

    deleteRate(rate: Rate): Observable<Rate> {
        //delete via api
        return Observable.of(rate);
    }


    //handles insert and update
    saveProductInventory(inventoryItem: ProductInventory): Observable<ProductInventory> {

        //save to api
        //if rateId is empty or '0', it's an insert
        if (inventoryItem.productInventoryId == '' || inventoryItem.productInventoryId == '0')
            inventoryItem.productInventoryId = this.createRandomId();

        return Observable.of(inventoryItem);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
        //keep close to real life (having ids in the models).
    }

    deleteProductInventory(inventoryItem: ProductInventory): Observable<ProductInventory> {
        //delete via api
        return Observable.of(inventoryItem);
    }

    saveLocationInventory(inventoryItem: LocationInventory): Observable<LocationInventory> {

        //save to api
        //if rateId is empty or '0', it's an insert
        if (inventoryItem.locationInventoryId == '' || inventoryItem.locationInventoryId == '0')
            inventoryItem.locationInventoryId = this.createRandomId();

        return Observable.of(inventoryItem);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
        //keep close to real life (having ids in the models).
    }

    deleteLocationInventory(inventoryItem: LocationInventory): Observable<LocationInventory> {
        //delete via api
        return Observable.of(inventoryItem);
    }

    saveBlackout(blackout: Blackout): Observable<Blackout> {

        //save to api
        //if rateId is empty or '0', it's an insert
        if (blackout.locationBlackoutId == '' || blackout.locationBlackoutId == '0')
            blackout.locationBlackoutId = this.createRandomId();

        return Observable.of(blackout);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
        //keep close to real life (having ids in the models).
    }

    deleteBlackout(blackout: Blackout): Observable<Blackout> {
        //delete via api
        return Observable.of(blackout);
    }

    saveSalesChannel(salesChannel: SalesChannel): Observable<SalesChannel> {
        //save to api
        //if rateId is empty or '0', it's an insert
        if (salesChannel.salesChannelId == '' || salesChannel.salesChannelId == '0')
            salesChannel.salesChannelId = this.createRandomId();

        return Observable.of(salesChannel);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
        //keep close to real life (having ids in the models).
    }


    deleteSalesChannel(salesChannel: SalesChannel): Observable<SalesChannel> {
        //delete via api
        return Observable.of(salesChannel);
    }

    saveUser(user: User): Observable<User> {

        //save to api
        //if rateId is empty or '0', it's an insert
        if (user.userId == '' || user.userId == '0')
            user.userId = this.createRandomId();

        return Observable.of(user);                //in real db setup, this productId would be set by the api/DB.  We set it here manually just 
        //keep close to real life (having ids in the models).
    }



    getRoles(): Observable<Array<string>> {
        let roles = [
            'Admin',
            'Call Center',
            'Location Manager',
            'RedemptionUser',
            'Restricted'
        ];
        return Observable.of(roles);
    }
}