﻿import { Constants } from "../common/constants";
import * as _ from 'underscore';
import { SalesChannel } from "./index";

export class SalesChannelProduct {
    public salesChannelProductId: string;
    public salesChannelId: string;
    public productId: string;
    public salesChannel: SalesChannel;
   
    constructor() {
    }
}
