﻿import { RateNameByLanguage } from "./rate-name-by-language";
import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class TimedRate {
    timedRateId: string;
    from: number;
    to: number;
    period: string;

    sundayValue: number;
    mondayValue: number;
    tuesdayValue: number;
    wednesdayValue: number;
    thursdayValue: number;
    fridayValue: number;
    saturdayValue: number;

    public get complete():boolean {
        if (this.from == 0 ||
            this.to == 0 ||
            this.period == '') {
            return false;
        }

        if (this.sundayValue <= 0 &&
            this.mondayValue <= 0 &&
            this.tuesdayValue <= 0 &&
            this.wednesdayValue <= 0 &&
            this.thursdayValue <= 0 &&
            this.fridayValue <= 0 &&
            this.saturdayValue <= 0) {
            return false;
        }

        return true;
    }

    constructor() {
        this.from = 0;
        this.to = 0;
        this.period = "";
        this.sundayValue = 0;
        this.mondayValue = 0;
        this.tuesdayValue = 0;
        this.wednesdayValue = 0;
        this.thursdayValue = 0;
        this.fridayValue = 0;
        this.saturdayValue = 0;
    }
}
