﻿import { LocationInstruction } from "./location-instruction";
import { LocationEmailSetup } from "./location-email-setup";
import { PassTypeOption } from "./pass-type-option";
import { Product } from "./product";

export class CommunicationSetup {

    public communicationSetupId: string;
    public locationId: string;
    public productId: string;
    public passUri: string;

    public passTypes: Array<string>;
    public instructions: Array<LocationInstruction>;
    public emailSetups: Array<LocationEmailSetup>;


    public emailSubject: string;
    public emailFrom: string;
    public emailBcc: string;

    public emailConfirmOption: string;
    public emailAmendmentOption: string;
    public emailCancelOption: string;

    public messageConfirmOptions: Array<PassTypeOption>;
    public messageAmendmentOption: string;
    public messageCancelOption: string;

    constructor() {
    }
}
