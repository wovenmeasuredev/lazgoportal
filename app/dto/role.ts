﻿import * as _ from 'underscore';

export class Role {
    roleId: string;
    roleName: string;    
    permissions: Array<string>;

    constructor() {
        this.permissions = new Array<string>();
        this.roleId = "0";
    }
}
