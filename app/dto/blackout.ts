﻿import { RateNameByLanguage } from "./rate-name-by-language";
import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class Blackout {
    public locationId: string;
    public locationBlackoutId: string;
    public startDateTime: Date;
    public endDateTime: Date;
    public reason: string;

    public get complete(): boolean {
        if (this.startDateTime &&
            this.endDateTime &&
            this.reason.length > 0) {
            return true;
        }
        return false;
    }


    constructor() {
        this.reason = "";
        this.locationBlackoutId = "0" 
    }
}
