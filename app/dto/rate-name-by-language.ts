﻿import { Language } from './language';

export class RateNameByLanguage {
    public rateNameByLanguageId: string;
    public language: Language;
    public rateName: string;

    constructor() {
        this.rateNameByLanguageId = "0";
        this.rateName = "";
    }
}