﻿import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class AdjustmentReason {
    public adjustmentReasonId: string;
    public reason: string;
    public edit: boolean;

    constructor() {
        this.reason = "";
        this.adjustmentReasonId = "0";
        this.edit = false;
    }
}
