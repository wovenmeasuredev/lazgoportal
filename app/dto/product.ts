﻿import { Rate, CommunicationSetup, SalesChannel } from "./index";
import { ProductInventory } from "./product-inventory";
import { SalesChannelProduct } from "./sales-channel-product";

export class Product {
    public productId: string;
    public name: string;
    public subName: string;
    public type: string;
    public status: string;
    public activeFrom: Date;
    public activeForever: boolean;
    public activeTo: Date;
    public rateImageUri: string;
    public shortDescription: string;
    public longDescription: string;
    public cancellationPolicy: string;
    public hardPassRequirement: string;

    public externalProductId: string;
    public minStayUnit: string;
    public minStayValue: number;
    public minLeadTimeValue: number;
    public minLeadTimeUnit: string;
    public maxLeadTimeValue: number;
    public maxLeadTimeUnit: string;

    public isCorporateRate: boolean;
    public isAdminRate: boolean;

    public allowZeroValue: boolean;
    public zeroValue: number;

    public numberOfVehicles: number;
    public salesChannelKey: string;

    public rates: Array<Rate>;

    public salesChannels: Array<SalesChannelProduct>;


    public customizeCommunications: boolean;
    public customizeInventory: boolean;
    public communicationSetup: CommunicationSetup;

    public inventory: Array<ProductInventory>;

    constructor() {
        this.productId = "0";
        this.name = "";
        this.subName = "";
        this.type = "";
        this.status = "";
        this.shortDescription = "";
        this.longDescription = "";
        this.cancellationPolicy = "";
        this.hardPassRequirement = "";
        this.communicationSetup = new CommunicationSetup();
    }

    public get complete(): boolean {
        if (this.type == '' ||
            this.subName == '' ||
            this.status == '' ||
            this.name == '' ||
            this.shortDescription == '' ||
            this.longDescription == '' ||
            this.cancellationPolicy == '' ||
            this.hardPassRequirement == '') {
            return false;
        }

        if (!this.activeFrom)
            return false;

        if (!this.activeForever && !this.activeTo)
            return false;


        return true;
    }
}
