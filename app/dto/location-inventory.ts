﻿import { Rate, CommunicationSetup } from "./index";
import { InventoryBase } from "./inventory";

export class LocationInventory extends InventoryBase {
    public locationId: string;
    public locationInventoryId: string;
   

    constructor() {
        super();
        this.locationId = "0";   
        this.locationInventoryId = "0";
    }   
    
}
