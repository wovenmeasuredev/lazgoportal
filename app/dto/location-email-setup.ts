﻿
export class LocationEmailSetup {
    public locationEmailSetupId: string;
    public locationEmailType: string;
    public subject: string;
    public from: string;
    public bcc: string;
    public body: string;
    constructor() {
    }
}
