﻿import { LocationInstruction } from "./location-instruction";
import { LocationEmailSetup } from "./location-email-setup";
import { PassTypeOption } from "./pass-type-option";
import { Product } from "./product";
import { CommunicationSetup } from "./communication-setup";
import { ProductInventory } from "./product-inventory";
import * as _ from 'underscore';
import { LocationInventory } from "./location-inventory";
import { Blackout } from "./blackout";

export class LocationDetail {
    public locationId: string;
    public locationName: string;
    public streetAddress: string;
    public city: string;
    public state: string;
    public zip: string;
    public generalPhone: string;
    public helpPhone: string;
    public email: string;
    public longitude: number;
    public latitude: number

    public organizationId: string;
    public organization: string;
    public regionId: string;
    public region: string;
    public eDataID: string;
    public GLCode: string;
    public statusId: string;
    public status: string;
    public users: string;

    public communicationSetup: CommunicationSetup;

    public products: Array<Product>; 

    public inventory: Array<LocationInventory>;

    public blackouts: Array<Blackout>;

    public get productInventory(): Array<ProductInventory> {
        let retVal = new Array<ProductInventory>();
        _.each(this.products, function (pi: Product) {
            retVal = retVal.concat(pi.inventory);
        });
        return retVal;
    }

    constructor() {
        this.communicationSetup = new CommunicationSetup();
        this.inventory = new Array<LocationInventory>();
        this.blackouts = new Array<Blackout>();
    }
}
