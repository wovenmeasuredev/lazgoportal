﻿import { RateNameByLanguage } from "./rate-name-by-language";
import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class FlatRate {
    sundayValue: number;
    mondayValue: number;
    tuesdayValue: number;
    wednesdayValue: number;
    thursdayValue: number;
    fridayValue: number;
    saturdayValue: number;

    public get complete(): boolean {
        if (this.sundayValue <= 0 &&
            this.mondayValue <= 0 &&
            this.tuesdayValue <= 0 &&
            this.wednesdayValue <= 0 &&
            this.thursdayValue <= 0 &&
            this.fridayValue <= 0 &&
            this.saturdayValue <= 0) {
            return false;
        }
        return true;
    }


    constructor() {
        this.sundayValue = 0;
        this.mondayValue = 0;
        this.tuesdayValue = 0;
        this.wednesdayValue = 0;
        this.thursdayValue = 0;
        this.fridayValue = 0;
        this.saturdayValue = 0;
    }
}
