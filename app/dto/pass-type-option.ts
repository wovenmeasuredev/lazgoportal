﻿export class PassTypeOption {
    public passType: string;
    public option: string;
   
    constructor(pt: string, opt: string) {
        this.passType = pt;
        this.option = opt;
    }
}
