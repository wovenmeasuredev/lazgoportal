﻿import { Language } from './language';
import { LocationInstructionByLanguage } from './location-instruction-by-language';

export class LocationInstruction {
    public sequence: number;
    public instructionId: string;
    public type: string;
    public typeId: number;
    public sectionId: number;
    public instructionByLanguage: Array<LocationInstructionByLanguage>;

    constructor() {
        this.instructionByLanguage = new Array<LocationInstructionByLanguage>();
    }
}
