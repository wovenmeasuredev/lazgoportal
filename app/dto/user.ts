﻿import * as _ from 'underscore';
import { Role } from "./index";

export class User {
    userId: string;
    userName: string;
    fullName: string;
    password: string;
    confirmPassword: string;
    roles: Array<Role>;
    organizations: Array<string>;
    locationOrganizations: Array<string>;

    public get complete(): boolean {
      
        return true;
    }



    constructor() {
        this.roles = new Array<Role>();
        this.organizations = new Array<string>();
        this.locationOrganizations = new Array<string>();
        this.userId = "0";
    }
}
