﻿import { RateNameByLanguage } from "./rate-name-by-language";
import { Constants } from "../common/constants";
import * as _ from 'underscore';
import { User } from "./user";
import { LanguageField } from "./language-field";
import { CommunicationSetup } from "./communication-setup";
import { ExportSetting } from "./export-setting";

export class Organization {
    public organizationId: string;
    public parentOrganizationId: string;
    public name: string;
    public type: string;
    public streetAddress: string;
    public city: string;
    public state: string;
    public zip: string;
    public siteCode: string;
    public dailyExportLastExportDate: Date;
    public fullfillmentDate: Date;

    public contactPerson: string;
    public contactEmail: string;
    public contactPhone: string;
    public currencySymbol: string;
    public callCenterReservationUrl: string;
    public parkingPassUrl: string;
    public internalPromoCodeExpiresEmail: string;
    public document: string;
    public customerCareEmail: string;
    public autoRenewDays: number;
    public autoRenewPeriod: number;
    public logoUri: string;

    /*Languages*/
    public english: boolean;
    public spanish: boolean;
    public french: boolean;
    public german: boolean;
    public customizeLanguageFields: boolean;

    /*Required Fields*/
    public customizeRequiredFields: boolean;
    public requiredCustomerSalutation: boolean;
    public requiredVehicleName: boolean;
    public requiredMobileNumberMandantory: boolean;
    public requiredFirstName: boolean;
    public requiredVehicleMake: boolean;
    public requiredCreditCardOnAccountMandantory: boolean;
    public requiredLastName: boolean;
    public requiredVehicleModel: boolean;
    public requiredAddress1: boolean;
    public requiredVehicleColor: boolean;
    public requiredAddress2: boolean;
    public requiredLicensePlateState: boolean;
    public requiredCity: boolean;
    public requiredLicensePlateNumber: boolean;
    public requiredState: boolean;
    public requiredInboundFlight: boolean;
    public requiredCountry: boolean;
    public requiredOutboundFlight: boolean;
    public requiredZip: boolean;
    public requiredMobileNumber: boolean;


    public languageFields: Map<string, Array<LanguageField>>;

    public communicationSetup: CommunicationSetup;

    public exportSetting: ExportSetting;

    public users: Array<User>;

    public locationId: string;

    public get isLocation(): boolean {
        return this.locationId && this.locationId != '';
    }

    public get complete(): boolean {
     
        return true;
    }


    constructor() {
        this.parentOrganizationId = "0";
        this.organizationId = "0";
        this.locationId = '';
        this.users = new Array<User>();
        this.languageFields = new Map<string, Array<LanguageField>>();
        this.communicationSetup = new CommunicationSetup();
        this.exportSetting = new ExportSetting();
    }
}
