﻿import { Language } from './language';

export class LocationInstructionByLanguage {
    public locationInstructionByLanguageId: string;
    public language: Language;
    public instruction: string;

    public constructor() {
        this.locationInstructionByLanguageId = "0";
    }
}