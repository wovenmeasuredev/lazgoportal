﻿import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class SalesChannel {
    public salesChannelId: string;
    public name: string;
    public amount: number;
    public amountType: string;
    public includeInRate: boolean;
    public active: boolean;
    public minCapFee: number;
    public maxCapFee: number;
    public feeType: string;

    public get complete(): boolean {
        if (this.name.length > 0 && this.amount > 0.0 && this.feeType.length > 0 ) {
            return true;
        }
        return false;
    }


    constructor() {
        this.name = "";
        this.salesChannelId = "0" 
        this.feeType = "";
        this.minCapFee = 0;
        this.maxCapFee = 0;
        this.includeInRate = false;
        this.active = true;
    }
}
