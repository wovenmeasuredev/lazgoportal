﻿import { Constants } from "../common/constants";
import * as _ from 'underscore';

export class ExportSetting {
    public organizationId: string;
    public dailyExportSystemAddress: string;
    public fulfillmentExportSystemAddress: string;
    public dailyExportSystemUserName: string;
    public fulfillmentExportSystemUserName: string;
    public dailyExportSystemPassword: string;
    public fullfillmentExportSystemPassword: string;
    public dailyExportSystemProtocol: string;
    public fulfillmentExportSytemProtocol: string;
    public dailyExportSystemPort: string;
    public fulfillmentExportSystemPort: string;

    constructor() {
    }
}
