﻿export class LanguageField {
    public code: string;
    public labelName: string;
    public translation: string;
    public comment: string;
    constructor() {
    }
}
