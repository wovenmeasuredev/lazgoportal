﻿import { Rate, CommunicationSetup } from "./index";

export class InventoryBase {
    public startDateTime: Date;
    public endDateTime: Date;
    public sunday: boolean;
    public monday: boolean;
    public tuesday: boolean;
    public wednesday: boolean;
    public thursday: boolean;
    public friday: boolean;
    public saturday: boolean;
    public spaces: number;
    public comment: string;

    constructor() {
        this.spaces = 0;
    }

    public get complete(): boolean {
        if (!this.startDateTime ||
            !this.endDateTime ||
            this.spaces <= 0) {
            return false;
        }

        if (!this.sunday &&
            !this.monday &&
            !this.tuesday &&
            !this.wednesday &&
            !this.thursday &&
            !this.friday &&
            !this.saturday) {
            return false;
        }

        return true;
    }

    
}
