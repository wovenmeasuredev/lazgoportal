﻿import { Language } from './language';
import { LocationInstructionByLanguage } from "./location-instruction-by-language";
import * as _ from 'underscore';

export class LocationCannedInstructionByLanguage {
    public sequence: number;
    public instructionByLanguage: Array<LocationInstructionByLanguage>;

    public getLanguagePhrase(languageCode: string): string {
        let retVal: string = "";
        _.each(this.instructionByLanguage, function (l: LocationInstructionByLanguage) {
            if (l.language.code == languageCode) {
                retVal = l.instruction;
            }
        });
        return retVal;
    }

    constructor() {
        this.instructionByLanguage = new Array<LocationInstructionByLanguage>();
    }
}
