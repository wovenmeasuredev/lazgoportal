﻿import { Rate, CommunicationSetup } from "./index";
import { InventoryBase } from "./inventory";

export class ProductInventory extends InventoryBase {
    public productId: string;
    public productName: string;
    public productInventoryId: string;
   

    constructor() {
        super();
        this.productId = "0";   
        this.productInventoryId = "0";
    }   
    
}
