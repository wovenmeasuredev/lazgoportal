﻿import { RateNameByLanguage } from "./rate-name-by-language";
import { Constants } from "../common/constants";
import * as _ from 'underscore';
import { FlatRate } from "./flat-rate";
import { TimedRate } from "./timed-rate";

export class Rate {
    public rateId: string; 
    public priority: number;
    public typeId: number;

    public get type(): string {
        if (this.typeId == (new Constants()).RateTypeFlat)
            return "Flat";
        else
            return "Timed";
    }

    public rateNameByLanguage: Array<RateNameByLanguage>;
    public startHour: number;
    public startMinute: number;
    public startAMPM: string;
    public endHour: number;
    public endMinute: number;
    public endAMPM: string;
    public sameDayNextDay: string;

    public sunday: boolean;
    public monday: boolean;
    public tuesday: boolean;
    public wednesday: boolean;
    public thursday: boolean;
    public friday: boolean;
    public saturday: boolean;

    public flatRate: FlatRate;
    public timedRates: Array<TimedRate>;

    public get defaultName() : string {
        let retVal: string = "";
        _.each(this.rateNameByLanguage, function (l: RateNameByLanguage) {
            if (l.language.code == (new Constants()).LanguageEnglish.code) {
                retVal = l.rateName;
            }
        });
        return retVal;
    } 

    constructor() {
        this.rateId = "0";
        this.rateNameByLanguage = new Array<RateNameByLanguage>();
        this.flatRate = new FlatRate();
        this.timedRates = new Array<TimedRate>();
        this.typeId = 0;
        this.startHour = 0;
        this.startMinute = 0;
        this.startAMPM = "";
        this.endHour = 0;
        this.endMinute = 0;
        this.endAMPM = "";
        this.sameDayNextDay = "";
    }
}
