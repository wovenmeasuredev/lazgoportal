﻿import { LocationInstruction } from "./location-instruction";
import { LocationEmailSetup } from "./location-email-setup";

export class LocationLite {
    public locationId: string;
    public locationName: string;
    public city: string;
    public state: string;
    public eDataID: string;
    public GLCode: string;

    constructor() {
    }
}
