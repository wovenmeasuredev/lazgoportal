﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';
import * as _ from 'underscore';
import { Product, Rate } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { RateTimedPriceComponent } from "./rate-timed-price";
import { RateFlatPriceComponent } from "./rate-flat-price";

@Component({ 
    templateUrl: 'add-edit-rate.html'
})
export class AddEditRateComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {
        this.rate = new Rate();
        let rbl = new RateNameByLanguage();
        rbl.language = this.constants.LanguageEnglish;
        this.rate.rateNameByLanguage.push(rbl);
        rbl = new RateNameByLanguage();
        rbl.language = this.constants.LanguageSpanish;
        this.rate.rateNameByLanguage.push(rbl);      
        this.step = 1;
    }   


    @ViewChild(RateTimedPriceComponent) rateTimedComponent: RateTimedPriceComponent;
    @ViewChild(RateFlatPriceComponent) rateFlatComponent: RateFlatPriceComponent;

    private rate: Rate;
    private step: number;
    private selectedProduct: Product;

    ngOnInit() {
        
    }
    
    saveRate() {
        this.dataService.saveRate(this.rate).subscribe(
            data => {
                this.activeModal.close(data);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    }

    close() {
        this.activeModal.close();
    }


    currentStepComplete() {
        let bOk: boolean = true;
        if (this.step == 1) {
            if (this.rate.typeId<=0)
                return false;

            _.each(this.rate.rateNameByLanguage, function (rbl: RateNameByLanguage) {
                if (rbl.rateName == '')
                    bOk = false;
            });

            return bOk;
        }
        else if (this.step == 2) {
            if (this.rate.startHour <= 0 ||
                this.rate.startMinute < 0 ||
                this.rate.startAMPM == '' ||
                this.rate.endHour <= 0 ||
                this.rate.endMinute < 0 ||
                this.rate.endAMPM == '' ||
                this.rate.sameDayNextDay == '')
                return false;

            if (!this.rate.sunday && !this.rate.monday && !this.rate.tuesday && !this.rate.wednesday && !this.rate.thursday &&
                !this.rate.friday && !this.rate.saturday) {
                return false;
            }

            return bOk;
        }

        else if (this.step == 3) {
            if (!this.rateFlatComponent && !this.rateTimedComponent)
                return false;

            if (this.rate.typeId == this.constants.RateTypeFlat) {
                return this.rateFlatComponent.isComplete();
            }
            else if (this.rate.typeId == this.constants.RateTypeTimed)
                return this.rateTimedComponent.isComplete();

        }
    }   
}