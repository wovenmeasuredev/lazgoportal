﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import { LookupService } from '../common/services/lookup-service';
import { Constants } from "../common/constants";
import { DataService } from "../common/services/data-service";
import { ProxyService } from "../common/services/proxy-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message, ConfirmationService, ConfirmDialogModule, SelectItem } from 'primeng/primeng';
import { Product, LocationDetail, LocationEmailSetup, LocationInstruction, LocationCannedInstructionByLanguage, LocationInstructionByLanguage, Rate } from '../dto';
import * as _ from 'underscore';
import { AddInstructionComponent } from './add-instruction';
import { SendRequestComponent } from './send-request';
import { AddEditProductComponent } from './add-edit-product';
import { AddEditRateComponent } from "./add-edit-rate";
import { AddEditInventoryComponent } from "./add-edit-inventory";
import { ProductInventory } from "../dto/product-inventory";
import { LocationInventory } from "../dto/location-inventory";
import { Blackout } from "../dto/blackout";
import { AddEditBlackoutComponent } from "./add-edit-blackout";


@Component({
    templateUrl: 'location-detail.html',
    providers: [ConfirmationService]
})
export class LocationDetailComponent {   
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private lookupService: LookupService,
                private ngbModal: NgbModal,
                private translationService: TranslationService,
                private dataService: DataService,
                private confirmationService: ConfirmationService) {

        this.productOptions = [];
        this.productOptions.push({ label: 'Current Products', value: 'c' });
        this.productOptions.push({ label: 'Past Products', value: 'p' });
        this.productOptions.push({ label: 'Active Products', value: 'a' });
        this.productOptions.push({ label: 'Inactive Products', value: 'i' });

        this.selectedProductOptions = new Array<string>();
        this.selectedProductOptions.push("c");
    }    


    msgs: Message[] = [];
    locationId: any;
    location: LocationDetail;  
    loaded: boolean = false;
    states: SelectItem[];
    regions: SelectItem[];
    addModal: any;
    emailCustom: any;
    color: string;
    selectedProduct: Product;
    selectedRate: Rate;
    productOptions: SelectItem[];
    selectedProductOptions: Array<string>;

    ngOnInit() {
        this.contextService.currentSection = "locations";
        this.locationId = this.route.snapshot.queryParams['i'];
        this.loadLocationDetail();
        this.loadStates();
    }   

    loadStates() { 
        this.states = [];
        this.states.push({ label: 'Select State', value: null });
        for (let n: number = 0; n < this.constants.US_States.length; n++) {
            this.states.push({ label: this.constants.US_States[n].state, value: this.constants.US_States[n].postCode });
        }
    }

    filterProducts() {
        //filter location.products
    }

    
    loadLocationDetail() {
        this.spinnerService.postStatus('Loading Location');
        this.dataService.getLocation(this.locationId).subscribe(
            data => {
                this.location = data;
            },
            (err) => { console.log(err); }, 
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    }   

    addProduct() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditProductComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;

        this.addModal.result.then(function (product: any) {
            if (product) {
                console.log(product);
                _self.location.products.push(product);
            }
        });
    }

    editProduct(product: Product) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditProductComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.product = product;

        this.addModal.result.then(function (product: any) {
            if (product) {
                _self.selectedProduct = product;
            }
        });
    }

    removeProduct(product: Product) {
        this.dataService.deleteProduct(product).subscribe(
            data => {
                let index: number = this.location.products.indexOf(product);
                this.location.products.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    }

    addRate() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditRateComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.selectedProduct = this.selectedProduct;
        this.addModal.result.then(function (rate: any) {
            if (rate) {
                console.log(rate);
                _self.selectedProduct.rates.push(rate);
            }
        });
    }

    editRate(rate: Rate) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditRateComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.selectedProduct = this.selectedProduct;
        contentComponentInstance.rate = rate;       
    }

    removeRate(rate: Rate) {
        this.dataService.deleteRate(rate).subscribe(
            data => {
                let index: number = this.selectedProduct.rates.indexOf(rate);
                this.selectedProduct.rates.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }

    addProductInventory() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditInventoryComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.selectedProduct = this.selectedProduct;
        contentComponentInstance.inventoryItem = new ProductInventory();
        contentComponentInstance.inventoryItem.productName = this.selectedProduct.name;
        contentComponentInstance.inventoryItem.productIde = this.selectedProduct.productId;

        this.addModal.result.then(function (inventory: ProductInventory) {
            if (inventory) {
                console.log(inventory);
                _self.selectedProduct.inventory.push(inventory);
            }
        });
    }


    editProductInventoryItem(inventory: ProductInventory) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditInventoryComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.selectedProduct = this.selectedProduct;
        contentComponentInstance.inventoryItem = inventory;
        contentComponentInstance.inventoryItem.productName = this.selectedProduct.name;

    }

    removeProductInventoryItem(inventory: ProductInventory) {
        this.dataService.deleteProductInventory(inventory).subscribe(
            data => {
                let index: number = this.selectedProduct.inventory.indexOf(inventory);
                this.selectedProduct.inventory.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }


    addLocationInventory() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditInventoryComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.inventoryItem = new LocationInventory();
        contentComponentInstance.inventoryItem.locationId = this.locationId;

        this.addModal.result.then(function (inventory: LocationInventory) {
            if (inventory) {
                console.log(inventory);
                _self.location.inventory.push(inventory);
            }
        });
    }


    editLocationInventoryItem(inventory: LocationInventory) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditInventoryComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.inventoryItem = inventory;
    }

    removeLocationInventoryItem(inventory: LocationInventory) {
        this.dataService.deleteLocationInventory(inventory).subscribe(
            data => {
                let index: number = this.location.inventory.indexOf(inventory);
                this.location.inventory.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }

    addBlackout() {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditBlackoutComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.blackout = new Blackout();
        contentComponentInstance.blackout.locationId = this.locationId;

        this.addModal.result.then(function (blackout: Blackout) {
            if (blackout) {
                console.log(blackout);
                _self.location.blackouts.push(blackout);
            }
        });
    }

    editBlackout(blackout: Blackout) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddEditBlackoutComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.blackout = blackout;

        this.addModal.result.then(function (blackout: Blackout) {
            _self.dataService.saveBlackout(blackout).subscribe(
                data => {
                   
                },
                (err) => { console.log(err); },
                () => {
                    _self.spinnerService.finishCurrentStatus();
                });
        });


    }

    removeBlackout(blackout: Blackout) {
        this.dataService.deleteBlackout(blackout).subscribe(
            data => {
                let index: number = this.location.blackouts.indexOf(blackout);
                this.location.blackouts.splice(index, 1);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }
}