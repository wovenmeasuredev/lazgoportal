﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { Product, Rate } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { FlatRate } from "../dto/flat-rate";
import { Blackout } from "../dto/blackout";

@Component({ 
    templateUrl: 'blackouts.html',
    selector: 'blackouts'
})
export class BlackoutsComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private translationService: TranslationService) {
        this.editingBlackout = new EventEmitter<Blackout>();
        this.deletingBlackout = new EventEmitter<Blackout>();
        this.addingBlackout = new EventEmitter<any>();
    }

    @Input() blackouts: Array<Blackout>;
    @Output() addingBlackout: EventEmitter<any>;
    @Output() editingBlackout: EventEmitter<Blackout>;
    @Output() deletingBlackout: EventEmitter<Blackout>;

    @ViewChild(DataTable) dataTable: DataTable;
    selectedBlackout: Blackout;
    
    ngOnInit() {
       
    }  

    addBlackout() {
        this.addingBlackout.emit();
    }

    editBlackout(blackout: Blackout) {
        this.selectedBlackout = blackout;
        this.editingBlackout.emit(blackout);

    }

    removeBlackout(blackout: Blackout) {
        this.deletingBlackout.emit(blackout);
    }
        
}