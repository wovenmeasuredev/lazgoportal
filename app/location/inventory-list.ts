﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import { LookupService } from '../common/services/lookup-service';
import { Constants } from "../common/constants";
import { DataService } from "../common/services/data-service";
import { ProxyService } from "../common/services/proxy-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message, ConfirmationService, ConfirmDialogModule, SelectItem } from 'primeng/primeng';
import { Product, CommunicationSetup, LocationDetail, LocationEmailSetup, LocationInstruction, LocationCannedInstructionByLanguage, LocationInstructionByLanguage, Rate } from '../dto';
import * as _ from 'underscore';
import { AddInstructionComponent } from './add-instruction';
import { SendRequestComponent } from './send-request';
import { AddEditProductComponent } from './add-edit-product';
import { AddEditRateComponent } from "./add-edit-rate";
import { ProductInventory } from "../dto/product-inventory";
import { LocationInventory } from "../dto/location-inventory";
import { InventoryBase } from "../dto/inventory";


@Component({
    templateUrl: 'inventory-list.html',
    providers: [ConfirmationService],
    selector: 'inventorylist'
})
export class InventoryListComponent {   
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private lookupService: LookupService,
                private ngbModal: NgbModal,
                private translationService: TranslationService,
                private dataService: DataService,
                private confirmationService: ConfirmationService) {

        this.readOnly = false;
        this.showProductName = false;      
    }    

    @Input()
    productInventory: Array<ProductInventory>;

    @Input()
    locationInventory: Array<LocationInventory>;

    @Input()
    inventoryMode: number; //1 = product, 2 = location

    @Input()
    showProductName: boolean;

    @Input()
    readOnly: boolean;

    @Output()
    addingInventory: EventEmitter<any> = new EventEmitter();

    @Output()
    editingInventory: EventEmitter<InventoryBase> = new EventEmitter();

    @Output()
    removingInventory: EventEmitter<InventoryBase> = new EventEmitter();
    
    ngOnInit() {
       
    }  

    getBinding(): Array<any> {
        if (this.productInventory)
            return this.productInventory;

        return this.locationInventory;
    }

    addInventory() {
        this.addingInventory.emit();
    }

    editInventoryItem(item: InventoryBase) {
        this.editingInventory.emit(item);
    }

    removeInventoryItem(item: InventoryBase) {
        this.removingInventory.emit(item);
    }

}