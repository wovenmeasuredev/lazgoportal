﻿//our root app component
import {Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import { LookupService } from '../common/services/lookup-service';
import { Constants } from "../common/constants";
import { DataService } from "../common/services/data-service";
import { ProxyService } from "../common/services/proxy-service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Message, ConfirmationService, ConfirmDialogModule, SelectItem } from 'primeng/primeng';
import { Product, CommunicationSetup, LocationDetail, LocationEmailSetup, LocationInstruction, LocationCannedInstructionByLanguage, LocationInstructionByLanguage, Rate } from '../dto';
import * as _ from 'underscore';
import { AddInstructionComponent } from './add-instruction';
import { SendRequestComponent } from './send-request';
import { AddEditProductComponent } from './add-edit-product';
import { AddEditRateComponent } from "./add-edit-rate";


@Component({
    templateUrl: 'communications.html',
    providers: [ConfirmationService],
    selector: 'communications'
})
export class CommunicationComponent {   
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private lookupService: LookupService,
                private ngbModal: NgbModal,
                private translationService: TranslationService,
                private dataService: DataService,
                private confirmationService: ConfirmationService) {
      
    }    

    @Input()
    setup: CommunicationSetup;

    msgs: Message[] = [];
    selectedInstructionType: string;
    addModal: any;
    emailCustom: any;
    color: string;

    ngOnInit() {
       
    }   

    saveInstruction(instruction) {
        instruction.edit = false;
    }

    removeInstruction(instruction: LocationInstruction) {
        var index = this.setup.instructions.indexOf(instruction);
        this.setup.instructions.splice(index, 1);
        for (var x = index; x < this.setup.instructions.length; x++)
        {
            if (this.setup.instructions[x].sectionId == instruction.sectionId && this.setup.instructions[x].sequence > instruction.sequence) {
                this.setup.instructions[x].sequence--;
            }
        }
    }  

    getFilteredInstructions(sectionId: number, typeId: string) {
        let ntypeId:number = parseInt(typeId);
        var filtered = _.where(this.setup.instructions, { typeId: ntypeId, sectionId: sectionId });
        return _.sortBy(filtered, "sequence");
    }

    getFilteredInstructionsBySection(sectionId: number) {
        var filtered = _.where(this.setup.instructions, { sectionId: sectionId });
        return _.sortBy(filtered, "sequence");
    }
    
    addInstruction(sectionId: number) {
        var _self = this;
        this.addModal = this.ngbModal.open(AddInstructionComponent, {
            backdrop: 'static', keyboard: false, size: 'lg', windowClass: 'modalWindow'
        });

        const contentComponentInstance = this.addModal.componentInstance;
        contentComponentInstance.sectionId = sectionId;

        this.addModal.result.then(function (result: any) {
            let li: LocationCannedInstructionByLanguage = result.li;
            let selectedTypeId: string = result.typeId;
            let locationInstruction = new LocationInstruction();
            let instructionByLanguage = new LocationInstructionByLanguage();
            instructionByLanguage.language = _self.constants.LanguageEnglish;
            instructionByLanguage.instruction = li.getLanguagePhrase("en");
            locationInstruction.instructionByLanguage.push(instructionByLanguage);

            instructionByLanguage = new LocationInstructionByLanguage();
            instructionByLanguage.language = _self.constants.LanguageSpanish;
            instructionByLanguage.instruction = li.getLanguagePhrase("es");
            locationInstruction.instructionByLanguage.push(instructionByLanguage);
            locationInstruction.sectionId = contentComponentInstance.sectionId;

            let maxSequence: number = 0;
            if (locationInstruction.sectionId != _self.constants.LocationInstructionSectionLocationInstructions)
                maxSequence = _.max(_self.getFilteredInstructionsBySection(contentComponentInstance.sectionId), function (instruction: LocationInstruction) { return instruction.sequence; }).sequence;
            else
                maxSequence = _.max(_self.getFilteredInstructions(contentComponentInstance.sectionId, selectedTypeId), function (instruction: LocationInstruction) { return instruction.sequence; }).sequence;

            locationInstruction.sequence = maxSequence + 1;
            if (locationInstruction.sectionId == _self.constants.LocationInstructionSectionLocationInstructions) {
                locationInstruction.typeId = parseInt(selectedTypeId);
            }
            _self.setup.instructions.push(locationInstruction);
        });
    }

    setSelectedPassType(type: string) {
        if (_.contains(this.setup.passTypes, type)) {
            this.setup.passTypes.splice(this.setup.passTypes.indexOf(type), 1);
        }
        else {
            if (type == this.constants.PassTypeNoPassRequired) {
                this.setup.passTypes = new Array<string>();
                this.setup.passTypes.push(type);
            }
            else {
                this.setup.passTypes.push(type);
            }
        }
    }

    isSelectedPassTypeSelected(type: string) {
        return (_.contains(this.setup.passTypes, type));
    }

    confirmEmailChange($event) {
        this.setup.emailConfirmOption = $event.target.value;
        if (this.setup.emailConfirmOption == this.constants.EmailOptionCustom) {
            let title: string = "CUSTOM CONFIRMATION EMAIL";
            let body: string = "The request for the creation of a custom confirmation email has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";
            this.confirm(title, body).then((x) => {
                if (!x) {
                    this.setup.emailConfirmOption == this.constants.EmailOptionDefault;
                }
                console.log(x);
            });
        }
    }

    confirmAmendmentChange($event) {
        this.setup.emailAmendmentOption = $event.target.value;
        if (this.setup.emailAmendmentOption == this.constants.EmailOptionCustom) {
            let title: string = "CUSTOM AMENDMENT EMAIL";
            let body: string = "The request for the creation of a custom amendment email has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";

            this.confirm(title, body).then((x) => {
                if (!x) {
                    this.setup.emailAmendmentOption == this.constants.EmailOptionDefault;
                }
                console.log(x);
            });
        }
    }

    confirmCancelChange($event) {
        this.setup.emailCancelOption = $event.target.value;
        if (this.setup.emailCancelOption == this.constants.EmailOptionCustom) {
            let title: string = "CUSTOM CANCELLATION EMAIL";
            let body: string = "The request for the creation of a custom cancellation email has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";


            this.confirm(title, body).then((x) => {
                if (!x) {
                    this.setup.emailCancelOption == this.constants.EmailOptionDefault;
                }
                console.log(x);
            });
        }
    }

    confirm(title: string, body: string) {
        var _self = this;

        return new Promise((resolve, reject) => {

            this.emailCustom = this.ngbModal.open(SendRequestComponent, {
                backdrop: 'static', keyboard: false, windowClass: 'modalWindow'
            });

            const contentComponentInstance = this.emailCustom.componentInstance;
            contentComponentInstance.title = title;
            contentComponentInstance.prompt = body;

            this.emailCustom.result.then(function (result: any) {
                resolve(result);
            });
        });
    }

    confirmMessageChange($event) {
        if ($event.target.value == this.constants.MessageOptionCustom) {
            let title: string = "CUSTOM CONFIRMATION MESSAGE";
            let body: string = "The request for the creation of a custom confirmation MESSAGE has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";
            this.confirm(title, body).then((x) => {
                console.log(x);
            });
        }

    }

    confirmMessageAmendmentChange($event) {
        this.setup.messageAmendmentOption = $event.target.value;
        if (this.setup.messageAmendmentOption == this.constants.EmailOptionCustom) {
            let title: string = "CUSTOM AMENDMENT MESSAGE";
            let body: string = "The request for the creation of a custom amendment MESSAGE has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";

            this.confirm(title, body).then((x) => {
                if (!x) {
                    this.setup.messageAmendmentOption == this.constants.MessageOptionDefault;
                }
                console.log(x);
            });
        }
    }

    confirmMessageCancelChange($event) {
        this.setup.messageCancelOption = $event.target.value;
        if (this.setup.messageCancelOption == this.constants.EmailOptionCustom) {
            let title: string = "CUSTOM CANCELLATION MESSAGE";
            let body: string = "The request for the creation of a custom cancellation email has been sent to an Admin.  A follow-up email will be sent.  Please add a note to explain your request:";


            this.confirm(title, body).then((x) => {
                if (!x) {
                    this.setup.messageCancelOption == this.constants.MessageOptionDefault;
                }
                console.log(x);
            });
        }
    }

}