﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({ 
    templateUrl: 'send-request.html'
})
export class SendRequestComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {

    }   

    comment: string;
    @Input() title: string;
    @Input() prompt: string;

    ngOnInit() {
        this.comment = "";
    }

    close() {
        this.activeModal.close();
    }

    send() {
        this.activeModal.close(this.comment);
    }

}