﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { LocationLite, LocationCannedInstructionByLanguage } from '../dto';

@Component({ 
    templateUrl: 'add-instruction.html'
})
export class AddInstructionComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {

    }   

    selectedInstructionType: number;
    public suggestions: Array<LocationCannedInstructionByLanguage>;
    @Input() sectionId: number;

    ngOnInit() {
        this.selectedInstructionType = 1;
        this.suggestions = new Array<LocationCannedInstructionByLanguage>();
        this.loadSampleInstructions();
    }

    loadSampleInstructions() {
        this.spinnerService.postStatus('Loading Instruction Phrases.');
        this.dataService.getInstructionSuggestions().subscribe(
            data => {
                this.suggestions = data;
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });
    }   


    close() {
        this.activeModal.close();
    }

    instructionSelected(li: LocationCannedInstructionByLanguage) {
        this.activeModal.close({ li: li, typeId: this.selectedInstructionType });
    }

}