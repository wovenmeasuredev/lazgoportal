﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { Product } from '../dto';

@Component({ 
    templateUrl: 'add-edit-product.html'
})
export class AddEditProductComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {
        this.product = new Product();
        this.step = 1;
    }   

    private product: Product;
    private step: number;

    ngOnInit() {      
    }
    
    saveProduct() {
        this.dataService.saveProduct(this.product).subscribe(
            data => {
                this.activeModal.close(data);
            },
            (err) => { console.log(err); },
            () => {
                this.spinnerService.finishCurrentStatus();
            });    
    }

    close() {
        this.activeModal.close();
    }
    

}