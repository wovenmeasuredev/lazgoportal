﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';
import * as _ from 'underscore';
import { Product, Rate } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { RateTimedPriceComponent } from "./rate-timed-price";
import { RateFlatPriceComponent } from "./rate-flat-price";
import { ProductInventory } from "../dto/product-inventory";
import { InventoryBase } from "../dto/inventory";
import { LocationInventory } from "../dto/location-inventory";
import { Blackout } from "../dto/blackout";

@Component({ 
    templateUrl: 'add-edit-blackout.html'
})
export class AddEditBlackoutComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private activeModal: NgbActiveModal,
                private translationService: TranslationService) {
        this.blackout = new Blackout();
      
    }   
    
    private blackout: Blackout;

    ngOnInit() {
        
    }
    
    saveBlackout() {
        if (this.blackout) {
            this.dataService.saveBlackout(this.blackout).subscribe(
                data => {
                    this.activeModal.close(data);
                },
                (err) => { console.log(err); },
                () => {
                    this.spinnerService.finishCurrentStatus();
                });
        }     
    }

    close() {
        this.activeModal.close();
    }


}