﻿//our root app component
import { Component, ComponentRef, ViewChild, ComponentFactoryResolver, ViewContainerRef, Input, Output, EventEmitter} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {AuthenticationService} from '../common/services/authentication-service';
import { SpinnerService } from '../common/services/spinner-service';
import { TranslationService } from '../common/services/translation-service';
import { ContextService } from '../common/services/context-service';
import {Constants } from "../common/constants";
import { ProxyService } from "../common/services/proxy-service";
import { DataService } from "../common/services/data-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SelectItem, Message, DataTable } from 'primeng/primeng';

import { Product, Rate } from '../dto';
import { RateNameByLanguage } from "../dto/rate-name-by-language";
import { FlatRate } from "../dto/flat-rate";
import { TimedRate } from "../dto/timed-rate";
import * as _ from 'underscore';

@Component({ 
    templateUrl: 'rate-timed-price.html',
    selector: 'rate-timed-price'
})
export class RateTimedPriceComponent {    
    constructor(private router: Router, 
                private componentFactoryResolver: ComponentFactoryResolver,
                private proxyService: ProxyService,
                private dataService: DataService,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private spinnerService: SpinnerService,
                private contextService: ContextService,
                private constants: Constants,
                private translationService: TranslationService) {
        this.rateChanged = new EventEmitter<Array<TimedRate>>();
    }

    @Input() timedRates: Array<TimedRate>;
    @Input() rate: Rate;
    @Output() rateChanged: EventEmitter<Array<TimedRate>>;
    
    ngOnInit() {
       
    }  

    onRateValueChanged() {
        this.rateChanged.emit(this.timedRates);
    }

    addTimedPrice() {
        this.timedRates.push(new TimedRate());
    }

    removeTimedPrice(timedRate: TimedRate) {
        this.timedRates.splice(this.timedRates.indexOf(timedRate), 1);
    }

    isComplete() {

        if (this.timedRates.length == 0)
            return false;

        let bOk: Boolean = true;
        _.each(this.timedRates, function (t: TimedRate) {
            if (!t.complete)
                bOk = false;
        });

        return bOk;
    }
}